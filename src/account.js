const Hash = require('./hash.js')
const RedisList = require('./redis-list.js')
const RedisObject = require('./redis-object.js')
const Timestamp = require('./timestamp.js')
const UUID = require('./uuid.js')

module.exports = {
  assignAdministrator,
  authenticate,
  cancelDelete,
  changeUsername,
  create,
  deleteAccount,
  generateID,
  identify,
  isUniqueUsername,
  load,
  loadMany,
  regenerateSessionKey,
  revokeAdministrator,
  scheduleDelete
}

/**
 * Generates a new accountid by encoding a Redis incrementing counter
 */
async function generateID () {
  const id = await UUID.generateID()
  return `account_${id}`
}

/**
 * Identifies a user accountid from their username
 * @param {string} username - the account username
 */
async function identify (username) {
  if (!username || !username.length) {
    throw new Error('invalid-username')
  }
  const usernameHash = Hash.fixedSaltHash(username)
  const accountid = await global.redisClient.hgetAsync('map:usernames', usernameHash)
  if (!accountid) {
    throw new Error('invalid-username')
  }
  return load(accountid)
}

/**
 * Confirms a username and password are correct
 * @param {string} username - the account username
 * @param {string} password - the account password
 */
async function authenticate (username, password) {
  if (!username || !username.length) {
    throw new Error('invalid-username')
  }
  if (!password || !password.length) {
    throw new Error('invalid-password')
  }
  if (global.minimumUsernameLength > username.length) {
    throw new Error('invalid-username-length')
  }
  if (global.minimumPasswordLength > password.length) {
    throw new Error('invalid-password-length')
  }
  const usernameHash = Hash.fixedSaltHash(username)
  const accountid = await global.redisClient.hgetAsync('map:usernames', usernameHash)
  if (!accountid) {
    throw new Error('invalid-username')
  }
  const passwordHash = await global.redisClient.hgetAsync(accountid, 'passwordHash')
  if (!passwordHash) {
    throw new Error('invalid-password')
  }
  const validPassword = Hash.randomSaltCompare(password, passwordHash)
  if (!validPassword) {
    throw new Error('invalid-password')
  }
  const account = await load(accountid)
  if (!account) {
    throw new Error('invalid-account')
  }
  return account
}

/**
 * Creates an account with a nominated username and password
 * @param {string} username - the account username
 * @param {string} password - the account password
 */
async function create (username, password) {
  if (!username || !username.length) {
    throw new Error('invalid-username')
  }
  if (!password || !password.length) {
    throw new Error('invalid-password')
  }
  if (global.minimumUsernameLength > username.length) {
    throw new Error('invalid-username-length')
  }
  if (global.minimumPasswordLength > password.length) {
    throw new Error('invalid-password-length')
  }
  const otherUsersExist = await global.redisClient.existsAsync('map:usernames')
  const usernameHash = Hash.fixedSaltHash(username)
  const uniqueUsername = await isUniqueUsername(usernameHash)
  if (!uniqueUsername) {
    throw new Error('duplicate-username')
  }
  const accountid = await generateID()
  await global.redisClient.hsetAsync('map:usernames', usernameHash, accountid)
  const passwordHash = Hash.randomSaltHash(password)
  const fieldsAndValues = [
    'object', 'account',
    'accountid', accountid,
    'usernameHash', usernameHash,
    'passwordHash', passwordHash,
    'sessionKey', UUID.random(64),
    'sessionKeyNumber', 1,
    'created', Timestamp.now
  ]
  if (!otherUsersExist) {
    fieldsAndValues.push(
      'administrator', Timestamp.now,
      'owner', Timestamp.now
    )
  }
  await global.redisClient.hmsetAsync(accountid, fieldsAndValues)
  return accountid
}

/**
 * Updates the account username and username map
 * @constructor
 * @param {string} accountid - the account
 * @param {string} username - the new username with a fixed-salt hash
 */
async function changeUsername (accountid, usernameHash) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  if (!usernameHash || !usernameHash.length) {
    throw new Error('invalid-username-hash')
  }
  const unique = await isUniqueUsername(usernameHash)
  if (!unique) {
    throw new Error('duplicate-username')
  }
  const account = await load(accountid)
  if (!account || account.deleted) {
    throw new Error('invalid-account')
  }
  await global.redisClient.hdelAsync('map:usernames', account.usernameHash)
  await global.redisClient.hsetAsync('map:usernames', usernameHash, accountid)
  await global.redisClient.hsetAsync(accountid, 'usernameHash', usernameHash)
}

/**
 * Loads an account
 * @constructor
 * @param {string} accountid - the account
 * @param {boolean} ignoreDeletedAccounst - supress 'invalid-account' exception
 */
async function load (accountid, ignoreDeletedAccounts) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const account = await global.redisClient.hgetallAsync(accountid)
  if (!account) {
    if (ignoreDeletedAccounts) {
      return null
    }
    throw new Error('invalid-accountid')
  }
  for (const field in account) {
    if (account[field] === 'true') {
      account[field] = true
      continue
    } else if (account[field] === 'false') {
      account[field] = false
      continue
    }
    try {
      const intValue = parseInt(account[field], 10)
      if (intValue.toString() === account[field]) {
        account[field] = intValue
      }
    } catch (error) {
    }
  }
  return account
}

/**
 * Loads the account for each nominated accountid
 * @constructor
 * @param {string} accountids - the accounts
 * @param {boolean} ignoreDeletedAccounst - supress 'invalid-account' exception
 */
async function loadMany (accountids, ignoreDeletedAccounts) {
  if (!accountids || !accountids.length) {
    throw new Error('invalid-accountids')
  }
  const accounts = []
  for (const accountid of accountids) {
    const account = await load(accountid, ignoreDeletedAccounts)
    if (account) {
      accounts.push(account)
    }
  }
  return accounts
}

/**
 * Marks the account for deletion
 * @param {string} accountid - the account
 */
async function scheduleDelete (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const account = await load(accountid)
  if (!account) {
    throw new Error('invalid-accountid')
  }
  if (account.deleted) {
    throw new Error('invalid-account')
  }
  const delay = global.deleteDelay < 1 ? 0 : global.deleteDelay * 24 * 60 * 60
  await global.redisClient.hsetAsync(accountid, 'deleted', Timestamp.now + delay - 1)
  return true
}

/**
 * Removes the deletion date from an account if it has not passed
 * @param {string} accountid - the account
 */
async function cancelDelete (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const account = await load(accountid)
  if (!account) {
    throw new Error('invalid-accountid')
  }
  if (!account.deleted) {
    throw new Error('invalid-account')
  }
  if (account.deleted < Timestamp.now) {
    throw new Error('invalid-account')
  }
  await global.redisClient.lremAsync(`deleted:accounts`, 1, accountid)
  await global.redisClient.hdelAsync(accountid, 'deleted')
  return true
}

/**
 * Deletes the account data from Redis
 * @param {string} accountid - the account
 */
async function deleteAccount (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const account = await load(accountid)
  if (!account) {
    throw new Error('invalid-accountid')
  }
  await global.redisClient.hdelAsync('map:usernames', account.usernameHash)
  await RedisList.remove('accounts', accountid)
  if (account.deleted) {
    await RedisList.remove(`deleted:accounts`, accountid)
  }
  if (account.administrator) {
    await RedisList.remove('administrator:accounts', accountid)
  }
  await global.redisClient.delAsync(accountid)
}

/**
 * Verifies a username is not in use
 * @param {string} accountid - the account
 * @param {string} property - the field
 */
async function isUniqueUsername (usernameHash) {
  if (!usernameHash || !usernameHash.length) {
    throw new Error('invalid-username-hash')
  }
  const exists = await global.redisClient.hexistsAsync('map:usernames', usernameHash)
  return !exists
}

/**
 * Marks the account as an administrator
 * @param {string} accountid - the account
 */
async function assignAdministrator (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const account = await load(accountid)
  if (!account) {
    throw new Error('invalid-accountid')
  }
  if (account.deleted || account.administrator) {
    throw new Error('invalid-account')
  }
  await RedisObject.setProperty(accountid, 'administrator', Timestamp.now)
}

/**
 * Removes the administrator status from an account`
 * @param {string} accountid - the account
 */
async function revokeAdministrator (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const account = await load(accountid)
  if (!account) {
    throw new Error('invalid-accountid')
  }
  if (account.deleted || !account.administrator) {
    throw new Error('invalid-account')
  }
  await RedisObject.removeProperty(accountid, 'administrator')
  await global.redisClient.lremAsync('administrator:accounts', 1, accountid)
}

async function regenerateSessionKey (accountid) {
  await RedisObject.setProperty(accountid, 'sessionKey', UUID.random(64))
  await RedisObject.setProperty(accountid, 'sessionKey_lastReset', Timestamp.now)
  await global.redisClient.hincrbyAsync(accountid, 'sessionKeyNumber', 1)
}
