/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../index.js')
const TestHelper = require('../test-helper.js')

describe('internal-api/account', () => {
  describe('Account#create()', () => {
    it('should require username', async () => {
      const username = null
      const password = 'password'
      let errorMessage
      try {
        await dashboard.Account.create(username, password)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
    })

    it('should force username length', async () => {
      const username = 'user-' + new Date().getTime()
      const password = 'password'
      global.minimumUsernameLength = 100
      let errorMessage
      try {
        await dashboard.Account.create(username, password)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username-length')
    })

    it('should require password', async () => {
      const username = 'user-' + new Date().getTime()
      const password = null
      let errorMessage
      try {
        await dashboard.Account.create(username, password)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password')
    })

    it('should force password length', async () => {
      const username = 'user-' + new Date().getTime()
      const password = '1'
      global.minimumPasswordLength = 100
      let errorMessage
      try {
        await dashboard.Account.create(username, password)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password-length')
    })

    it('should require unique username', async () => {
      const username = 'user-' + new Date().getTime() + '-' + Math.random()
      const password = 'password'
      await dashboard.Account.create(username, password)
      let errorMessage
      try {
        await dashboard.Account.create(username, password)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'duplicate-username')
    })
  })

  describe('Account#identify()', () => {
    it('should require username', async () => {
      let errorMessage
      try {
        await dashboard.Account.identify(null)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
    })

    it('should require valid username', async () => {
      const username = 'user-' + new Date().getTime()
      let errorMessage
      try {
        await dashboard.Account.identify(username)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
    })

    it('should load account', async () => {
      const user = await TestHelper.createUser()
      const identified = await dashboard.Account.identify(user.username)
      assert.strictEqual(user.account.accountid, identified.accountid)
    })
  })

  describe('Account#scheduleDelete', async () => {
    it('should require a valid accountid', async () => {
      let errorMessage
      try {
        await dashboard.Account.scheduleDelete()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should reject if scheduled already', async () => {
      const user = await TestHelper.createUser()
      await dashboard.Account.scheduleDelete(user.account.accountid)
      let errorMessage
      try {
        await dashboard.Account.scheduleDelete(user.account.accountid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should schedule the delete for 7 days', async () => {
      const user = await TestHelper.createUser()
      await dashboard.Account.scheduleDelete(user.account.accountid)
      global.deleteDelay = 7
      const account = await dashboard.Account.load(user.account.accountid)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((account.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, 7)
    })

    it('should schedule the delete for 3 days', async () => {
      const user = await TestHelper.createUser()
      global.deleteDelay = 3
      await dashboard.Account.scheduleDelete(user.account.accountid)
      const account = await dashboard.Account.load(user.account.accountid)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((account.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, 3)
    })
  })

  describe('Account#cancelDelete', () => {
    it('should require a valid accountid', async () => {
      let errorMessage
      try {
        await dashboard.Account.cancelDelete()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should reject if not scheduled', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.Account.cancelDelete(user.account.accountid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should reject after the date', async () => {
      const user = await TestHelper.createUser()
      global.deleteDelay = -1
      await dashboard.Account.scheduleDelete(user.account.accountid)
      let errorMessage
      try {
        await dashboard.Account.cancelDelete(user.account.accountid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should remove the deleted date', async () => {
      const user = await TestHelper.createUser()
      global.deleteDelay = 1
      await dashboard.Account.scheduleDelete(user.account.accountid)
      await dashboard.Account.cancelDelete(user.account.accountid)
      const deleted = await dashboard.RedisObject.getProperty(user.account.accountid, 'deleted')
      assert.strictEqual(deleted, undefined)
    })
  })

  describe('Account#deleteAccount', () => {
    it('should require a valid accountid', async () => {
      let errorMessage
      try {
        await dashboard.Account.deleteAccount()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should delete the account', async () => {
      const user = await TestHelper.createUser()
      global.deleteDelay = -1
      await TestHelper.setDeleted(user)
      await dashboard.Account.deleteAccount(user.account.accountid)
      let errorMessage
      try {
        await dashboard.Account.load(user.account.accountid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })
  })
})
