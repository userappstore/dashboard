const RedisObject = require('./redis-object.js')
const Session = require('./session.js')
const Timestamp = require('./timestamp.js')

module.exports = {
  wrapAPIRequest,
  generate: () => {
    const api = {}
    for (const url in global.sitemap) {
      if (url.indexOf('/api/') !== 0) {
        continue
      }
      const pathParts = url.substring(5).split('/')
      const prior = []
      for (const partRaw of pathParts) {
        let part = partRaw
        if (!prior.length) {
          api[part] = api[part] || {}
          prior.push(part)
          continue
        }
        let obj = api
        for (const priorPart of prior) {
          obj = obj[priorPart]
        }
        prior.push(part)
        if (prior.length === pathParts.length) {
          if (partRaw.indexOf('-') === -1) {
            part = partRaw.charAt(0).toUpperCase() + partRaw.substring(1)
          } else {
            const segments = partRaw.split('-')
            part = ''
            for (const segment of segments) {
              part += segment.charAt(0).toUpperCase() + segment.substring(1)
            }
          }
          obj[part] = global.sitemap[url].api
        } else {
          obj[part] = obj[part] || {}
        }
      }
      wrapAPIRequest(global.sitemap[url].api, url)
    }
    return api
  }
}

/**
 * wrapAPIRequest takes each of the HTTP-or-not API routes and wraps
 * a function that verifies access is allowed and the user allowed and
 * optionally ends a ClientResponse with JSON of returned data
 * @param {*} nodejsHandler an API endpoint
 */
function wrapAPIRequest (nodejsHandler, filePath) {
  for (const functionName of ['get', 'post', 'patch', 'delete', 'put', 'head', 'option']) {
    const originalFunction = nodejsHandler[functionName]
    if (!originalFunction) {
      continue
    }
    if (nodejsHandler[`_${functionName}`]) {
      continue
    }
    nodejsHandler[`_${functionName}`] = originalFunction
    nodejsHandler[functionName] = async (req, res) => {
      if (!req.session && nodejsHandler.auth !== false) {
        if (res) {
          res.statusCode = 511
          res.setHeader('content-type', 'application/json')
          return res.end(`{"message": "Sign in required"}`)
        }
        return { message: 'Sign in required' }
      }
      if (nodejsHandler.before) {
        await nodejsHandler.before(req)
      }
      if (nodejsHandler.lock) {
        if (req.session.lockURL !== req.url) {
          await Session.lock(req.session.sessionid, req.url)
          req.session = await Session.load(req.session.sessionid)
        }
        if (!req.session.unlocked) {
          await RedisObject.setProperty(req.session.sessionid, 'lockStarted', Timestamp.now)
          await RedisObject.setProperty(req.session.sessionid, 'lockData', req.body ? JSON.stringify(req.body) : '{}')
          if (res) {
            res.statusCode = 511
            res.setHeader('content-type', 'application/json')
            return res.end(`{"message": "Authorization required"}`)
          }
          return { message: 'Authorization required' }
        }
        await RedisObject.removeProperties(req.session.sessionid, ['lockStarted', 'lockData', 'lockURL', 'lock'])
        if (req.session.unlocked <= Timestamp.now) {
          await RedisObject.removeProperty(req.session.sessionid, 'unlocked')
          req.session = await Session.load(req.session.sessionid)
        }
      }
      let result
      try {
        result = await originalFunction(req)
      } catch (error) {
        if (res) {
          res.statusCode = 500
          res.setHeader('content-type', 'application/json')
          return res.end(`{"error": "${error.message}"}`)
        }
        throw error
      }
      if (res) {
        res.statusCode = 200
        return res.end(result ? JSON.stringify(result) : null)
      }
      return result
    }
  }
  return nodejsHandler
}
