const dashboard = require('../../index.js')

module.exports = {
  template: sessionImpersonationHeader
}

function sessionImpersonationHeader (req, res, templateDoc) {
  if (!req.account || !req.administratorAccount) {
    return
  }
  if (req.administratorAccount.accountid !== req.account.accountid) {
    dashboard.HTML.renderTemplate(templateDoc, req.session, 'session-impersonate', 'notifications-container')
  }
}
