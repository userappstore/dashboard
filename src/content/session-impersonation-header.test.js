/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../index.js')
const fs = require('fs')
const path = require('path')
const SessionImpersionationHeader = require('./session-impersonation-header.js')
const templateHTML = fs.readFileSync(path.join(__dirname, '../template.html')).toString('utf-8')
const TestHelper = require('../../test-helper.js')

describe(`content/session-impersonation-header`, () => {
  describe('SessionImpersionationHeader#AFTER', () => {
    it('should do nothing for non-administrator', async () => {
      const user = await TestHelper.createUser()
      const templateDoc = dashboard.HTML.parse(templateHTML)
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      await SessionImpersionationHeader.template(req, res, templateDoc)
      const notificationsContainer = templateDoc.getElementById('notifications-container')
      assert.strictEqual(undefined, notificationsContainer.child)
    })

    it('should do nothing when not impersonating', async () => {
      const administrator = await TestHelper.createAdministrator()
      const templateDoc = dashboard.HTML.parse(templateHTML)
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      await SessionImpersionationHeader.template(req, res, templateDoc)
      const notificationsContainer = templateDoc.getElementById('notifications-container')
      assert.strictEqual(undefined, notificationsContainer.child)
    })

    it('should add impersonation message to header', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      const templateDoc = dashboard.HTML.parse(templateHTML)
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.administratorAccount = administrator.administratorAccount
      req.administratorSession = administrator.administratorSession
      req.account = administrator.account
      req.session = administrator.session
      const res = TestHelper.createResponse()
      await SessionImpersionationHeader.template(req, res, templateDoc)
      const notificationsContainer = templateDoc.getElementById('notifications-container')
      assert.strictEqual(notificationsContainer.child.length, 1)
    })
  })
})
