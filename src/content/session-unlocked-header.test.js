/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../index.js')
const fs = require('fs')
const path = require('path')
const SessionUnlockedHeader = require('./session-unlocked-header.js')
const templateHTML = fs.readFileSync(path.join(__dirname, '../template.html')).toString('utf-8')
const TestHelper = require('../../test-helper.js')

describe(`content/session-unlocked-header`, () => {
  describe('SessionUnlockedHeader#AFTER', () => {
    it('should do nothing for not-unlocked accounts', async () => {
      const user = await TestHelper.createUser()
      const templateDoc = dashboard.HTML.parse(templateHTML)
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      await SessionUnlockedHeader.template(req, res, templateDoc)
      const notificationsContainer = templateDoc.getElementById('notifications-container')
      assert.strictEqual(undefined, notificationsContainer.child)
    })

    it('should add lock message to header', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      await TestHelper.unlockSession(user, true)
      const templateDoc = dashboard.HTML.parse(templateHTML)
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      await SessionUnlockedHeader.template(req, res, templateDoc)
      const notificationsContainer = templateDoc.getElementById('notifications-container')
      assert.strictEqual(notificationsContainer.child.length, 1)
    })
  })
})
