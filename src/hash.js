const bcrypt = require('bcrypt-node')

module.exports = {
  fixedSaltCompare,
  fixedSaltHash,
  randomSaltCompare,
  randomSaltHash
}

function fixedSaltCompare (text, hash) {
  return fixedSaltHash(text) === hash
}

function fixedSaltHash (text) {
  const finalText = text + (global.applicationEncryptionKey || '')
  const salt = global.bcryptFixedSalt || '$2a$10$uyrNLHlx/gxwbdSowtRP7u'
  const full = bcrypt.hashSync(finalText, salt)
  return full.substring(salt.length)
}

function randomSaltCompare (text, hash) {
  return bcrypt.compareSync(text + (global.applicationEncryptionKey || ''), hash)
}

function randomSaltHash (text) {
  const salt = bcrypt.genSaltSync(global.bcryptWorkloadFactor || 11)
  return bcrypt.hashSync(text + (global.applicationEncryptionKey || ''), salt)
}
