const RedisList = require('./redis-list.js')
const Timestamp = require('./timestamp.js')
const UUID = require('./uuid.js')

module.exports = {
  create,
  deleteAccount,
  deleteProfile,
  generateID,
  load,
  loadMany
}

/**
 * Generates a new accountid by encoding a Redis incrementing counter
 */
async function generateID () {
  const id = await UUID.generateID()
  return `profile_${id}`
}

async function deleteProfile (profileid) {
  if (!profileid || !profileid.length) {
    throw new Error('invalid-profileid')
  }
  const profile = await load(profileid)
  if (!profile) {
    throw new Error('invalid-profileid')
  }
  await global.redisClient.delAsync(profileid)
  return true
}

async function create (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const profileid = await generateID()
  const fieldsAndValues = [
    'object', 'profile',
    'accountid', accountid,
    'profileid', profileid,
    'created', Timestamp.now
  ]
  await global.redisClient.hmsetAsync(profileid, fieldsAndValues)
  return load(profileid)
}

async function load (profileid) {
  if (!profileid || !profileid.length) {
    throw new Error('invalid-profileid')
  }
  const profile = await global.redisClient.hgetallAsync(profileid)
  if (!profile) {
    throw new Error('invalid-profileid')
  }
  for (const field in profile) {
    try {
      const intValue = parseInt(profile[field], 10)
      if (intValue.toString() === profile[field]) {
        profile[field] = intValue
      }
    } catch (error) {

    }
  }
  return profile
}

async function loadMany (profileids) {
  if (!profileids || !profileids.length) {
    throw new Error('invalid-profileids')
  }
  const accounts = []
  for (const profileid of profileids) {
    const profile = await load(profileid)
    accounts.push(profile)
  }
  return accounts
}

async function deleteAccount (accountid) {
  if (!accountid) {
    throw new Error('invalid-accountid')
  }
  const profileids = await RedisList.listAll(`account:profiles:${accountid}`)
  if (!profileids || !profileids.length) {
    return
  }
  for (const profileid of profileids) {
    await RedisList.remove('profiles', profileid)
    await global.redisClient.delAsync(profileid)
  }
  return global.redisClient.delAsync(`account:profiles:${accountid}`)
}
