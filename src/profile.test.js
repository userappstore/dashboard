/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../index.js')
const TestHelper = require('../test-helper.js')

describe('internal-api/profile', () => {
  describe('Profile#deleteAccount', () => {
    it('should require an accountid', async () => {
      let errorMessage
      try {
        await dashboard.Profile.deleteAccount()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should delete the profile data', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createProfile(user)
      await dashboard.Profile.deleteAccount(user.account.accountid)
      let errorMessage
      try {
        await dashboard.Profile.load(user.profile.profileid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profileid')
    })
  })

  describe('Profile#deleteProfile()', () => {
    it('should require a profile', async () => {
      let errorMessage
      try {
        await dashboard.Profile.deleteProfile()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profileid')
    })

    it('should require a valid profile', async () => {
      let errorMessage
      try {
        await dashboard.Profile.deleteProfile('invalid')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profileid')
    })

    it('should delete the profile', async () => {
      const user = await TestHelper.createUser()
      const profile = await dashboard.Profile.create(user.account.accountid)
      await dashboard.Profile.deleteProfile(profile.profileid)
      let errorMessage
      try {
        await dashboard.Profile.load(profile.profileid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profileid')
    })
  })

  describe('Profile#load()', () => {
    it('should require profile', async () => {
      let errorMessage
      try {
        await dashboard.Profile.load()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profileid')
    })

    it('should return the profile', async () => {
      const user = await TestHelper.createUser()
      const profile = await dashboard.Profile.create(user.account.accountid)
      const loaded = await dashboard.Profile.load(profile.profileid)
      assert.strictEqual(profile.profileid, loaded.profileid)
      assert.strictEqual(user.account.accountid, loaded.accountid)
    })
  })

  describe('Profile#loadMany()', () => {
    it('should require one or more profileids', async () => {
      let errorMessage
      try {
        await dashboard.Profile.loadMany()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profileids')
    })

    it('should load the profiles', async () => {
      const user = await TestHelper.createUser()
      const profiles = [
        await dashboard.Profile.create(user.account.accountid),
        await dashboard.Profile.create(user.account.accountid),
        await dashboard.Profile.create(user.account.accountid)
      ]
      const profileids = [
        profiles[0].profileid,
        profiles[1].profileid,
        profiles[2].profileid
      ]
      const loaded = await dashboard.Profile.loadMany(profileids)
      assert.strictEqual(loaded.length, profileids.length)
      for (const i in profileids) {
        assert.strictEqual(loaded[i].profileid, profileids[i])
      }
    })
  })
})
