module.exports = {
  add: addItem,
  count: countCollection,
  exists: itemExists,
  list: listItems,
  listAll: listAllItems,
  remove: removeItem
}

async function itemExists (collection, itemid) {
  const exists = await global.redisClient.hgetAsync(`map:${collection}:id`, itemid)
  return exists === 1 || exists === '1'
}

async function addItem (collection, itemid) {
  const exists = await itemExists(collection, itemid)
  if (exists) {
    return
  }
  await global.redisClient.hsetAsync(`map:${collection}:id`, itemid, 1)
  return global.redisClient.lpushAsync(collection, itemid)
}

async function countCollection (collection) {
  return global.redisClient.llenAsync(collection)
}

async function listAllItems (collection) {
  return global.redisClient.lrangeAsync(collection, 0, -1)
}

async function listItems (collection, offset, pageSize) {
  offset = offset || 0
  pageSize = pageSize || parseInt(global.pageSize, 10)
  if (offset < 0) {
    throw new Error('invalid-offset')
  }
  const itemids = await global.redisClient.lrangeAsync(collection, offset, pageSize + offset - 1)
  if (!itemids || !itemids.length) {
    return null
  }
  for (const i in itemids) {
    try {
      const integer = parseInt(itemids[i], 10)
      if (integer.toString() === itemids[i]) {
        itemids[i] = integer
      }
    } catch (error) {
    }
  }
  return itemids
}

async function removeItem (collection, itemid) {
  await global.redisClient.hdelAsync(`map:${collection}:id`, itemid)
  return global.redisClient.lremAsync(collection, 1, itemid)
}
