/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../index.js')

describe('internal-api/redis-list', () => {
  describe('RedisList#add', () => {
    it('should add the item', async () => {
      await dashboard.RedisList.add('things', 1)
      const things = await dashboard.RedisList.list('things')
      assert.strictEqual(things.length, 1)
      assert.strictEqual(things[0], 1)
    })
  })

  describe('RedisList#count()', async () => {
    it('should count the items', async () => {
      await dashboard.RedisList.add('things', 1)
      await dashboard.RedisList.add('things', 2)
      await dashboard.RedisList.add('things', 3)
      const things = await dashboard.RedisList.count('things')
      assert.strictEqual(things, 3)
    })

    it('should not count removed items', async () => {
      await dashboard.RedisList.add('things', 1)
      await dashboard.RedisList.add('things', 2)
      await dashboard.RedisList.add('things', 3)
      await dashboard.RedisList.remove('things', 3)
      const things = await dashboard.RedisList.count('things')
      assert.strictEqual(things, 2)
    })
  })

  describe('RedisList#remove', () => {
    it('should not count removed items', async () => {
      await dashboard.RedisList.add('things', 3)
      await dashboard.RedisList.remove('things', 3)
      const things = await dashboard.RedisList.count('things')
      assert.strictEqual(things, 0)
    })
  })

  describe('RedisList#list()', () => {
    it('should enforce page size', async () => {
      global.pageSize = 3
      await dashboard.RedisList.add('things', 1)
      await dashboard.RedisList.add('things', 2)
      await dashboard.RedisList.add('things', 3)
      await dashboard.RedisList.add('things', 4)
      const listed = await dashboard.RedisList.list('things')
      assert.strictEqual(listed.length, global.pageSize)
      assert.strictEqual(listed[0], 4)
      assert.strictEqual(listed[1], 3)
      assert.strictEqual(listed[2], 2)
    })

    it('should enforce offset', async () => {
      await dashboard.RedisList.add('things', 1)
      await dashboard.RedisList.add('things', 2)
      await dashboard.RedisList.add('things', 3)
      const offset = 1
      const listed = await dashboard.RedisList.list('things', offset)
      assert.strictEqual(listed.length, global.pageSize)
      assert.strictEqual(listed[0], 2)
      assert.strictEqual(listed[1], 1)
    })
  })
})
