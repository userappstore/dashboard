module.exports = {
  getProperties,
  getProperty,
  removeProperty,
  removeProperties,
  setProperty,
  setProperties
}

/**
 * Retrieves multiple property from the object
 * @param {string} objectid - the object
 * @param {string} array - array of properties
 */
async function getProperties (objectid, array) {
  if (!objectid || !objectid.length) {
    throw new Error('invalid-objectid')
  }
  if (!array || !array.length) {
    throw new Error('invalid-array')
  }
  const data = await global.redisClient.hmgetAsync(objectid, array)
  const object = {}
  for (const i in data) {
    const key = array[i]
    let value = data[i]
    if (value === undefined || value === null) {
      object[key] = undefined
      continue
    }
    try {
      const intValue = parseInt(value, 10)
      if (intValue.toString() === value) {
        value = intValue
      }
    } catch (error) {
    }
    object[key] = value
  }
  return object
}

/**
 * Retrieves a property from the object
 * @param {string} objectid - the object
 * @param {string} property - name of property
 */
async function getProperty (objectid, property) {
  if (!objectid || !objectid.length) {
    throw new Error('invalid-objectid')
  }
  if (!property || !property.length) {
    throw new Error('invalid-property')
  }
  let value = await global.redisClient.hgetAsync(objectid, property)
  if (value === undefined || value === null) {
    return undefined
  }
  try {
    const intValue = parseInt(value, 10)
    if (intValue < 0 || intValue > 0 || intValue === 0) {
      value = intValue
    }
  } catch (error) {
  }
  return value
}

/**
 * Attaches multiple properties and values to the object
 * @param {string} objectid - the object
 * @param {string} hash - hash of properties
 */
async function setProperties (objectid, hash) {
  if (!objectid || !objectid.length) {
    throw new Error('invalid-objectid')
  }
  if (!hash) {
    throw new Error('invalid-hash')
  }
  const keys = Object.keys(hash)
  if (!keys.length) {
    throw new Error('invalid-hash')
  }
  const fieldsAndValues = []
  for (const key of keys) {
    const value = hash[key]
    if (!value) {
      throw new Error('invalid-value')
    }
    fieldsAndValues.push(key, hash[key])
  }
  return global.redisClient.hmsetAsync(objectid, fieldsAndValues)
}

/**
 * Attaches a property and value to the object
 * @param {string} objectid - the object
 * @param {string} property - the field
 * @param {string} value - the value
 */
async function setProperty (objectid, property, value) {
  if (!objectid || !objectid.length) {
    throw new Error('invalid-objectid')
  }
  if (!property || !property.length) {
    throw new Error('invalid-property')
  }
  if (value == null || value === undefined) {
    throw new Error('invalid-value')
  }
  await global.redisClient.hsetAsync(objectid, property, value)
}

/**
 * Removes multiple properties from the object
 * @param {string} objectid - the object
 * @param {string} array - array of properties
 */
async function removeProperties (objectid, array) {
  if (!objectid || !objectid.length) {
    throw new Error('invalid-objectid')
  }
  if (!array || !array.length) {
    throw new Error('invalid-array')
  }
  for (const item of array) {
    await global.redisClient.hdelAsync(objectid, item)
  }
}

/**
 * Removes a property from the object
 * @param {string} objectid - the object
 * @param {string} property - the field
 */
async function removeProperty (objectid, property) {
  if (!objectid || !objectid.length) {
    throw new Error('invalid-objectid')
  }
  if (!property || !property.length) {
    throw new Error('invalid-property')
  }
  await global.redisClient.hdelAsync(objectid, property)
}
