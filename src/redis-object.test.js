/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../index.js')
const TestHelper = require('../test-helper.js')

describe('internal-api/redis-object', () => {
  describe('RedisObject#setProperty', () => {
    it('should require an objectid', async () => {
      let errorMessage
      try {
        await dashboard.RedisObject.setProperty(null, 'property', 'value')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-objectid')
    })

    it('should require a property', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.setProperty(user.account.accountid, null, 'value')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-property')
    })

    it('should require a value', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.setProperty(user.account.accountid, 'property', null)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-value')
    })

    it('should set the property', async () => {
      const user = await TestHelper.createUser()
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testProperty', 'test-value')
      const value = await dashboard.RedisObject.getProperty(user.account.accountid, 'testProperty')
      assert.strictEqual(value, 'test-value')
    })
  })

  describe('RedisObject#setProperties', () => {
    it('should require an objectid', async () => {
      let errorMessage
      try {
        await dashboard.RedisObject.setProperties(null, {})
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-objectid')
    })

    it('should require a key/value hash', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.setProperties(user.account.accountid, null)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-hash')
    })

    it('should require a value', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.setProperties(user.account.accountid, { property: null })
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-value')
    })

    it('should set the properties', async () => {
      const user = await TestHelper.createUser()
      const data = {
        testProperty: 'testValue',
        testInteger: 1234,
        testDate: new Date()
      }
      await dashboard.RedisObject.setProperties(user.account.accountid, data)
      const testValue = await dashboard.RedisObject.getProperty(user.account.accountid, 'testProperty')
      assert.strictEqual(testValue, 'testValue')
      const testInteger = await dashboard.RedisObject.getProperty(user.account.accountid, 'testInteger')
      assert.strictEqual(testInteger, 1234)
      const testDate = await dashboard.RedisObject.getProperty(user.account.accountid, 'testDate')
      assert.strictEqual(dashboard.Timestamp.create(testDate), dashboard.Timestamp.create(testDate))
    })
  })

  describe('RedisObject#getProperty', () => {
    it('should require an objectid', async () => {
      let errorMessage
      try {
        await dashboard.RedisObject.getProperty(null, 'property', 'value')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-objectid')
    })

    it('should require a property', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.getProperty(user.account.accountid, null, 'value')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-property')
    })

    it('should retrieve the property', async () => {
      const user = await TestHelper.createUser()
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testProperty', 'test-value')
      const stringValue = await dashboard.RedisObject.getProperty(user.account.accountid, 'testProperty')
      assert.strictEqual(stringValue, 'test-value')
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testProperty', 1234)
      const testProperty = await dashboard.RedisObject.getProperty(user.account.accountid, 'testProperty')
      assert.strictEqual(testProperty, 1234)
    })
  })

  describe('RedisObject#getProperties', () => {
    it('should require an objectid', async () => {
      let errorMessage
      try {
        await dashboard.RedisObject.getProperties(null, ['property', 'property2'])
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-objectid')
    })

    it('should require a property array', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.getProperties(user.account.accountid, null)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-array')
    })

    it('should retrieve the properties', async () => {
      const user = await TestHelper.createUser()
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testProperty', 'test-value')
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testInteger', 1234)
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testDate', new Date())
      const data = await dashboard.RedisObject.getProperties(user.account.accountid, ['testProperty', 'testInteger', 'testDate'])
      assert.strictEqual(data.testProperty, 'test-value')
      assert.strictEqual(data.testInteger, 1234)
    })
  })

  describe('RedisObject#removeProperty', () => {
    it('should require an objectid', async () => {
      let errorMessage
      try {
        await dashboard.RedisObject.removeProperty(null, 'property', 'value')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-objectid')
    })

    it('should require a property', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.removeProperty(user.account.accountid, null, 'value')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-property')
    })

    it('should remove the property', async () => {
      const user = await TestHelper.createUser()
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testProperty', 'test-value')
      await dashboard.RedisObject.removeProperty(user.account.accountid, 'testProperty')
      const stringValue = await dashboard.RedisObject.getProperty(user.account.accountid, 'testProperty')
      assert.strictEqual(stringValue, undefined)
    })
  })

  describe('RedisObject#removeProperties', () => {
    it('should require an objectid', async () => {
      let errorMessage
      try {
        await dashboard.RedisObject.removeProperties(null, ['property', 'property2'])
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-objectid')
    })

    it('should require a property array', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.RedisObject.removeProperties(user.account.accountid, null)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-array')
    })

    it('should remove the properties', async () => {
      const user = await TestHelper.createUser()
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testProperty', 'test-value')
      await dashboard.RedisObject.setProperty(user.account.accountid, 'testInteger', 7890)
      const valuesBefore = await dashboard.RedisObject.getProperties(user.account.accountid, ['testProperty', 'testInteger'])
      assert.strictEqual(valuesBefore.testProperty, 'test-value')
      assert.strictEqual(valuesBefore.testInteger, 7890)
      await dashboard.RedisObject.removeProperties(user.account.accountid, ['testProperty', 'testInteger'])
      const valuesAfter = await dashboard.RedisObject.getProperties(user.account.accountid, ['testProperty', 'testInteger'])
      assert.strictEqual(valuesAfter.testProperty, undefined)
      assert.strictEqual(valuesAfter.testInteger, undefined)
    })
  })
})
