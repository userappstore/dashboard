const Redis = require('redis')
const EncryptedRedis = require('encrypted-redis')
const util = require('util')

module.exports = util.promisify((callback) => {
  let client = Redis.createClient(global.redisURL)
  client.on('error', (error) => {
    return process.exit(1)
  })
  client.on('end', () => {
    client = null
  })
  return callback(null, EncryptedRedis(client))
})
