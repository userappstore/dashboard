const Hash = require('./hash.js')
const RedisList = require('./redis-list.js')
const Timestamp = require('./timestamp.js')
const UUID = require('./uuid.js')

module.exports = {
  create,
  deleteAccount,
  deleteCode,
  generateID,
  load,
  loadMany,
  useCode
}

async function load (codeid, ignoreDeletedAccounts) {
  if (!codeid || !codeid.length) {
    throw new Error('invalid-codeid')
  }
  const code = await global.redisClient.hgetallAsync(codeid)
  if (!code) {
    throw new Error('invalid-codeid')
  }
  for (const field in code) {
    try {
      const intValue = parseInt(code[field], 10)
      if (intValue.toString() === code[field]) {
        code[field] = intValue
      }
    } catch (error) {

    }
  }
  return code
}

async function create (accountid, codeHash) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const codeid = await generateID()
  const fieldsAndValues = [
    'object', 'resetCode',
    `accountid`, accountid,
    `codeid`, codeid,
    `codeHash`, codeHash,
    `created`, Timestamp.now
  ]
  await global.redisClient.hsetAsync(`map:account:resetCodes:${accountid}`, codeHash, codeid)
  await global.redisClient.hmsetAsync(codeid, fieldsAndValues)
  return load(codeid)
}

async function generateID () {
  const id = await UUID.generateID()
  return `reset_${id}`
}

async function useCode (accountid, code) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  if (!code || !code.length) {
    throw new Error('invalid-reset-code')
  }
  const codeHash = Hash.fixedSaltHash(code)
  const codeid = await global.redisClient.hgetAsync(`map:account:resetCodes:${accountid}`, codeHash)
  if (!codeid) {
    throw new Error('invalid-reset-code')
  }
  await deleteCode(codeid)
  await global.redisClient.lremAsync('resetCodes', 1, codeid)
  return true
}

async function deleteCode (codeid) {
  if (!codeid || !codeid.length) {
    throw new Error('invalid-codeid')
  }
  const code = await load(codeid)
  await global.redisClient.lremAsync(`account:resetCodes:${code.accountid}`, 1, codeid)
  await global.redisClient.hdelAsync(`map:account:resetCodes:${code.accountid}`, code.codeHash)
  await global.redisClient.delAsync(codeid)
  await global.redisClient.lremAsync('resetCodes', 1, codeid)
  return true
}

async function loadMany (codeids, ignoreDeletedAccounts) {
  if (!codeids || !codeids.length) {
    throw new Error('invalid-codeids')
  }
  const codes = []
  for (let i = 0, len = codeids.length; i < len; i++) {
    const code = await load(codeids[i], ignoreDeletedAccounts)
    if (!code) {
      continue
    }
    codes.push(code)
  }
  return codes
}

async function deleteAccount (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const codeids = await RedisList.listAll(`account:resetCodes:${accountid}`)
  if (codeids && codeids.length) {
    for (const codeid of codeids) {
      await RedisList.remove('resetCodes', codeid)
    }
  }
  await global.redisClient.delAsync(`account:resetCodes:${accountid}`)
  await global.redisClient.delAsync(`map:account:resetCodes:${accountid}`)
}
