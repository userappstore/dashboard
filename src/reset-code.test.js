/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../index.js')
const TestHelper = require('../test-helper.js')

describe('internal-api/reset-code', () => {
  describe('ResetCode#create()', () => {
    it('should require accountid', async () => {
      let errorMessage
      try {
        await dashboard.ResetCode.create()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should create a code', async () => {
      const user = await TestHelper.createUser()
      const code = await dashboard.ResetCode.create(user.account.accountid, 'a-fake-code-dashboard.Hash')
      assert.strictEqual(code.object, 'resetCode')
    })
  })

  describe('ResetCode#deleteCode()', () => {
    it('should require a code', async () => {
      let errorMessage
      try {
        await dashboard.ResetCode.deleteCode()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-codeid')
    })

    it('should require a valid code', async () => {
      const user = await TestHelper.createUser()
      await dashboard.ResetCode.create(user.account.accountid, 'a-fake-code-dashboard.Hash')
      let errorMessage
      try {
        await dashboard.ResetCode.deleteCode('invalid')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-codeid')
    })

    it('should delete the code', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      await dashboard.ResetCode.deleteCode(user.resetCode.codeid)
      let errorMessage
      try {
        await dashboard.ResetCode.load(dashboard.ResetCode.codeid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-codeid')
    })
  })

  describe('ResetCode#load()', () => {
    it('should require code', async () => {
      let errorMessage
      try {
        await dashboard.ResetCode.load()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-codeid')
    })

    it('should return the code', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const loaded = await dashboard.ResetCode.load(user.resetCode.codeid)
      assert.strictEqual(user.resetCode.codeid, loaded.codeid)
      assert.strictEqual(user.account.accountid, loaded.accountid)
    })
  })

  describe('ResetCode#loadMany()', () => {
    it('should require one or more codeids', async () => {
      let errorMessage
      try {
        await dashboard.ResetCode.loadMany()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-codeids')
    })

    it('should load the codes', async () => {
      const user = await TestHelper.createUser()
      const codes = [
        await dashboard.ResetCode.create(user.account.accountid, dashboard.Hash.fixedSaltHash('1-this-is-a-code-' + new Date().getTime())),
        await dashboard.ResetCode.create(user.account.accountid, dashboard.Hash.fixedSaltHash('2-this-is-a-code-' + new Date().getTime())),
        await dashboard.ResetCode.create(user.account.accountid, dashboard.Hash.fixedSaltHash('3-this-is-a-code-' + new Date().getTime()))
      ]
      const codeids = [
        codes[0].codeid,
        codes[1].codeid,
        codes[2].codeid
      ]
      const loaded = await dashboard.ResetCode.loadMany(codeids)
      assert.strictEqual(loaded.length, codeids.length)
      for (const i in codeids) {
        assert.strictEqual(loaded[i].codeid, codeids[i])
      }
    })
  })

  describe('ResetCode#useCode()', () => {
    it('should require an accountid', async () => {
      let errorMessage
      try {
        await dashboard.ResetCode.useCode()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should require a code', async () => {
      const user = await TestHelper.createUser()
      let errorMessage
      try {
        await dashboard.ResetCode.useCode(user.account.accountid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-reset-code')
    })

    it('should require a valid code', async () => {
      const user = await TestHelper.createUser()
      const codeHash = dashboard.Hash.fixedSaltHash('1-this-is-a-code-' + new Date().getTime())
      await dashboard.ResetCode.create(user.account.accountid, codeHash)
      let errorMessage
      try {
        await dashboard.ResetCode.useCode(user.account.accountid, 'wrong code')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-reset-code')
    })

    it('should delete the used code', async () => {
      const user = await TestHelper.createUser()
      const rawCode = 'this-is-a-code-' + new Date().getTime()
      const codeHash = dashboard.Hash.fixedSaltHash(rawCode)
      const code = await dashboard.ResetCode.create(user.account.accountid, codeHash)
      await dashboard.ResetCode.useCode(user.account.accountid, rawCode)
      let errorMessage
      try {
        await dashboard.ResetCode.load(code.codeid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-codeid')
    })
  })

  describe('ResetCode#deleteAccount', () => {
    it('should require an accountid', async () => {
      let errorMessage
      try {
        await dashboard.ResetCode.deleteAccount()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should delete the reset code data', async () => {
      const user = await TestHelper.createUser()
      const codeHash = dashboard.Hash.fixedSaltHash('1-this-is-a-code-' + new Date().getTime())
      await dashboard.ResetCode.create(user.account.accountid, codeHash)
      await dashboard.ResetCode.deleteAccount(user.account.accountid)
      const codesNow = await dashboard.RedisList.list(`account:resetCodes:${user.account.accountid}`)
      assert.strictEqual(codesNow, null)
    })
  })
})
