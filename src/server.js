const Account = require('./account.js')
const fs = require('fs')
const Hash = require('./hash.js')
const HTML = require('./html.js')
const http = require('http')
const Proxy = require('./proxy.js')
const qs = require('querystring')
const RedisObject = require('./redis-object.js')
const Response = require('./response.js')
const Session = require('./session.js')
const Timestamp = require('./timestamp.js')
const url = require('url')
const util = require('util')

const parsePostData = util.promisify((req, callback) => {
  if (req.headers['content-type'] && req.headers['content-type'].startsWith('multipart/form-data')) {
    return callback()
  }
  if (!req.headers['content-length']) {
    return callback()
  }
  let body = ''
  req.on('data', (data) => {
    body += data
  })
  return req.on('end', () => {
    if (!body) {
      return callback()
    }
    return callback(null, body)
  })
})

let server
const fileCache = {}

module.exports = {
  authenticateRequest,
  parsePostData,
  receiveRequest,
  requestIPAddress,
  start,
  stop,
  staticFile
}

function start () {
  server = http.createServer(receiveRequest)
  server.listen(global.port, global.host)
  return server
}

function stop () {
  return server.close()
}

async function receiveRequest (req, res) {
  const question = req.url.indexOf('?')
  req.state = 'received'
  req.urlPath = question === -1 ? req.url : req.url.substring(0, question)
  const dot = req.urlPath.lastIndexOf('.')
  req.route = global.sitemap[`${req.urlPath}/index`] || global.sitemap[req.urlPath]
  req.extension = dot > -1 ? req.urlPath.substring(dot + 1) : null
  req.ip = requestIPAddress(req)
  req.userAgent = req.headers['user-agent']
  if (question !== -1) {
    req.query = url.parse(req.url, true).query
  }
  if ((req.method === 'POST' || req.method === 'PATCH' || req.method === 'PUT' || req.method === 'DELETE')) {
    req.bodyRaw = await parsePostData(req)
    if (req.bodyRaw) {
      req.body = qs.parse(req.bodyRaw)
    }
  }
  // disallow public API access unless it's explicitly enabled
  // or the application server making the request
  if (global.applicationServer && global.applicationServer === req.headers['x-application']) {
    const token = req.headers['x-token']
    const accountid = req.headers['x-accountid']
    const sessionid = req.headers['x-sessionid']
    const expected = `${global.applicationServerToken}:${accountid}:${sessionid}`
    if (Hash.randomSaltCompare(expected, token)) {
      req.applicationServer = true
    }
  }
  if (!req.applicationServer && req.headers['x-application']) {
    return Response.throw500(req, res)
  }
  if (req.url.startsWith('/api/') && !global.allowPublicAPI && !req.applicationServer) {
    return Response.throw404(req, res)
  }
  // public static files are served without authentication
  if (req.url.startsWith('/public/')) {
    if (req.method === 'GET') {
      return staticFile(req, res)
    } else {
      return Response.throw404(req, res)
    }
  }
  try {
    await executeHandlers(req, res, 'before', global.packageJSON.dashboard.server, global.packageJSON.dashboard.serverFilePaths)
  } catch (error) {
    if (error.message === 'invalid-route') {
      return Response.throw404(req, res)
    }
    return Response.throw500(req, res)
  }
  if (res.ended) {
    return
  }
  req.state = 'before-complete'
  // routes with APIs must support the method being requested
  if (req.route && req.route.api !== 'static-page') {
    const methodHandler = req.route.api[req.method.toLowerCase()]
    if (!methodHandler) {
      return Response.throw404(req, res)
    }
  }
  let user
  // the application server specifies the account holder
  if (req.applicationServer) {
    if (req.headers['x-accountid']) {
      const account = await Account.load(req.headers['x-accountid'])
      const session = await Session.load(req.headers['x-sessionid'])
      user = { account, session }
    }
    // otherwise use cookie-based authentiation
  } else {
    try {
      user = await authenticateRequest(req)
    } catch (error) {
    }
  }
  if (user) {
    req.session = user.session
    req.account = user.account
    // clearing old sessions
    if (req.session) {
      if (user.session.unlocked > 1 && user.session.unlocked < Timestamp.now) {
        await RedisObject.removeProperties(user.session.sessionid,
          ['lockStarted', 'lockData', 'lockURL', 'lock', 'unlocked'])
        req.session = await Session.load(user.session.sessionid)
      }
      // restoring locked session data
      if (req.url === req.session.lockURL && req.session.unlocked && req.session.lockData) {
        req.body = JSON.parse(req.session.lockData)
        await RedisObject.removeProperty(req.session.sessionid, 'lockData')
      }
      // restricting locked session URLs
      if (req.session.lock && !req.session.unlocked) {
        if (req.urlPath !== '/account/authorize' &&
          req.urlPath !== '/account/signout' &&
          req.urlPath !== '/api/user/set-session-unlocked') {
          return Response.redirect(req, res, '/account/authorize')
        }
      }
    }
    // administrators
    if (user.account.administrator) {
      req.administratorAccount = user.account
      req.administratorSession = user.session
      if (req.session.impersonate &&
        req.urlPath === '/administrator/end-impersonation') {
        req.session = await Session.load(req.session.impersonate)
        req.account = await Account.load(req.session.accountid)
      }
    }
  }
  // require signing in to continue
  if (!req.account && req.route && req.route.auth !== false) {
    return Response.redirectToSignIn(req, res)
  }
  // require administrators not impersonating another account to continue
  if (req.urlPath.startsWith('/administrator')) {
    if (!req.account) {
      return Response.redirectToSignIn(req, res)
    }
    if (!req.administratorAccount || req.administratorAccount.accountid === req.session.administrator) {
      return Response.throw500(req, res)
    }
  }
  // the 'after' handlers can see signed in users
  req.state = 'authenticated'
  try {
    await executeHandlers(req, res, 'after', global.packageJSON.dashboard.server, global.packageJSON.dashboard.serverFilePaths)
  } catch (error) {
    if (error.message === 'invalid-route') {
      return Response.throw404(req, res)
    }
    return Response.throw500(req, res)
  }
  if (res.ended) {
    return
  }
  req.state = 'after-complete'
  // if there's no route the request is passed to the application server
  req.route = global.sitemap[req.urlPath]
  if (!req.route) {
    if (global.applicationServer) {
      return Proxy.pass(req, res)
    } else {
      return Response.throw404(req, res)
    }
  }
  // static html pages
  if (req.route.api === 'static-page') {
    const doc = HTML.parse(req.route.html)
    return Response.end(req, res, doc)
  }
  // iframe of a URL
  if (req.route.iframe) {
    return Response.end(req, res)
  }
  // nodejs handler for the route
  req.state = 'route'
  return req.route.api[req.method.toLowerCase()](req, res)
}

async function executeHandlers (req, res, method, handlers) {
  if (!handlers || !handlers.length) {
    return
  }
  for (const handler of handlers) {
    if (!handler || !handler[method]) {
      continue
    }
    await handler[method](req, res)
    if (res.ended) {
      return
    }
  }
}

async function staticFile (req, res) {
  // root /public folder
  let filePath = `${global.rootPath}${req.urlPath}`
  if (!fs.existsSync(filePath)) {
    // dashboard /public folder
    filePath = `${global.applicationPath}/node_modules/@userappstore/dashboard/src/www${req.urlPath}`
    // module /public folder
    if (!fs.existsSync(filePath)) {
      for (const moduleName of global.packageJSON.dashboard.moduleNames) {
        filePath = `${global.applicationPath}/node_modules/${moduleName}/src/www/${req.urlPath}`
        if (fs.existsSync(filePath)) {
          break
        }
      }
    }
  }
  if (fs.existsSync(filePath)) {
    const stat = fs.statSync(filePath)
    if (stat.isDirectory()) {
      return Response.throw404(req, res)
    }
    fileCache[filePath] = fileCache[filePath] || fs.readFileSync(filePath)
    return Response.end(req, res, null, fileCache[filePath])
  }
  if (global.applicationServer) {
    return Proxy.pass(req, res)
  }
  return Response.throw404(req, res)
}

async function authenticateRequest (req) {
  if (!req.headers.cookie || !req.headers.cookie.length) {
    return
  }
  const segments = req.headers.cookie.split(';')
  const cookie = {}
  for (const segment of segments) {
    if (!segment || segment.indexOf('=') === -1) {
      continue
    }
    const parts = segment.split('=')
    const key = parts.shift().trim()
    const value = parts.join('=')
    cookie[key] = decodeURI(value)
  }
  if (!cookie) {
    return
  }
  if (!cookie.sessionid || !cookie.token) {
    throw new Error('invalid-cookie')
  }
  const session = await Session.load(cookie.sessionid)
  if (!session || session.ended) {
    throw new Error('invalid-session')
  }
  const account = await Account.load(session.accountid)
  if (!account || account.deleted) {
    throw new Error('invalid-account')
  }
  if (session.tokenHash !== Session.hashServerToken(account.accountid, cookie.token, account.sessionKey)) {
    throw new Error('invalid-cookie')
  }
  if (req.session && req.session.lock && !req.session.unlocked) {
    if (req.urlPath !== '/account/authorize') {
      throw new Error('invalid-session')
    }
  }
  return { session, account }
}

function requestIPAddress (req) {
  let xForwardFor = req.headers['x-forwarded-for']
  if (xForwardFor) {
    const comma = xForwardFor.indexOf(',')
    if (comma > -1) {
      return xForwardFor.substring(0, comma)
    }
    return xForwardFor
  }
  if (req.connection) {
    if (req.connection.remoteAddress) {
      return req.connection.remoteAddress
    } else if (req.connection.socket) {
      return req.connection.socket
    }
  }
  if (req.socket && req.socket.remoteAddress) {
    return req.socket.remoteAddress
  }
}
