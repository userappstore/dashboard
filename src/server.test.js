const assert = require('assert')
const http = require('http')
const Server = require('./server.js')
const TestHelper = require('../test-helper.js')

/* eslint-env mocha */
describe('internal-api/server', () => {
  describe('Server#authenticateRequest()', () => {
    it('should not return user', async () => {
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      let result
      try {
        await Server.authenticateRequest(req)
      } catch (error) {
      }
      assert.strictEqual(result, undefined)
    })

    it('should return user', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.headers.cookie = `sessionid=${user.session.sessionid}; token=${user.session.token}; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/`
      let result
      try {
        result = await Server.authenticateRequest(req)
      } catch (error) {
      }
      assert.strictEqual(result.account.accountid, user.account.accountid)
    })

    it('should not identify a user', async () => {
      const req = TestHelper.createRequest('/account/change-username', 'GET')
      req.headers.cookie = `sessionid=invalid; token=invalid`
      req.cookie = {
        sessionid: 'invalid',
        token: 'invalid'
      }
      let errorMessage
      try {
        await Server.authenticateRequest(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-sessionid')
    })

    it('should identify the user', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/change-username', 'GET')
      req.headers.cookie = `sessionid=${user.session.sessionid}; token=${user.session.token}`
      const authenticated = await Server.authenticateRequest(req)
      assert.strictEqual(authenticated.account.accountid, user.account.accountid)
      assert.strictEqual(authenticated.session.sessionid, user.session.sessionid)
    })
  })

  describe('Server#parsePostData()', () => {
    it('should ignore file uploads', async () => {
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.headers['content-type'] = 'multipart/form-data'
      req.headers['content-length'] = '1234'
      const bodyRaw = await Server.parsePostData(req)
      assert.strictEqual(bodyRaw, undefined)
    })

    it('should ignore no-content uploads', async () => {
      const requestOptions = {
        host: 'localhost',
        path: '/account/signin',
        port: process.env.PORT,
        method: 'POST',
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'content-length': 0
        }
      }
      const proxyRequest = http.request(requestOptions, (proxyResponse) => {
        let body = ''
        proxyResponse.on('data', (chunk) => {
          body += chunk
        })
        return proxyResponse.on('end', () => {
          const doc = TestHelper.extractDoc(body)
          const username = doc.getElementById('username')
          assert.strictEqual(username.attr.value, undefined)
        })
      })
      return proxyRequest.end()
    })

    it('should parse post data', async () => {
      const postData = 'username=invalid&password=nope'
      const requestOptions = {
        host: 'localhost',
        path: '/account/signin',
        port: process.env.PORT,
        method: 'POST',
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'content-length': postData.length
        }
      }
      const proxyRequest = http.request(requestOptions, (proxyResponse) => {
        let body = ''
        proxyResponse.on('data', (chunk) => {
          body += chunk
        })
        return proxyResponse.on('end', () => {
          const doc = TestHelper.extractDoc(body)
          const username = doc.getElementById('username')
          assert.strictEqual(username.attr.value, 'invalid')
        })
      })
      proxyRequest.write(postData)
      return proxyRequest.end()
    })
  })

  describe('Server#requestIPAddress()', () => {
    it('should bind x-forwarded-for to req', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.account = user.account
      req.session = user.session
      req.headers['x-forwarded-for'] = '1.2.3.4'
      req.connection = { remoteAddress: '4.5.6.7' }
      delete (req.ip)
      const ip = await Server.requestIPAddress(req)
      assert.strictEqual(ip, '1.2.3.4')
    })

    it('should bind first x-forwarded-for IP to req', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.account = user.account
      req.session = user.session
      req.headers['x-forwarded-for'] = '1.2.3.4,5.6.7.8'
      req.connection = { remoteAddress: '4.5.6.7' }
      delete (req.ip)
      const ip = await Server.requestIPAddress(req)
      assert.strictEqual(ip, '1.2.3.4')
    })

    it('should bind connection address for IP to req', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.account = user.account
      req.session = user.session
      req.connection = { remoteAddress: '4.5.6.7' }
      delete (req.ip)
      const ip = await Server.requestIPAddress(req)
      assert.strictEqual(ip, '4.5.6.7')
    })
  })

  describe('Server#receiveRequest()', () => {
    it('should bind query data of URL to req', async () => {
      const req = TestHelper.createRequest(`/account/change-username?param1=1&param2=this`, 'GET')
      delete (req.query)
      const res = TestHelper.createResponse()
      res.end = () => {
        assert.strictEqual(req.query.param1, '1')
        assert.strictEqual(req.query.param2, 'this')
      }
      return Server.receiveRequest(req, res)
    })

    it('should not bind route for unknown url', async () => {
      const req = TestHelper.createRequest(`/not-real`, 'GET')
      delete (req.route)
      const res = TestHelper.createResponse()
      res.end = () => {
        assert.strictEqual(req.route, undefined)
      }
      return Server.receiveRequest(req, res)
    })

    it('should bind route to req', async () => {
      const req = TestHelper.createRequest(`/account/change-username?param1=1&param2=this`, 'GET')
      delete (req.route)
      const res = TestHelper.createResponse()
      res.end = () => {
        assert.strictEqual(req.route.api, global.sitemap['/account/change-username'].api)
      }
      return Server.receiveRequest(req, res)
    })

    it('should bind user agent to req', async () => {
      const req = TestHelper.createRequest(`/account/change-username`, 'GET')
      req.headers['user-agent'] = 'This is a test'
      delete (req.userAgent)
      const res = TestHelper.createResponse()
      res.end = () => {
        assert.strictEqual(req.userAgent, 'This is a test')
      }
      return Server.receiveRequest(req, res)
    })
  })
})
