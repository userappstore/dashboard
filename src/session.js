const Hash = require('./hash.js')
const RedisList = require('./redis-list.js')
const Timestamp = require('./timestamp.js')
const UUID = require('./uuid.js')

module.exports = {
  create,
  deleteAccount,
  end,
  generateID,
  hashServerToken,
  load,
  loadMany,
  lock,
  unlock
}

async function generateID () {
  const id = await UUID.generateID()
  return `session_${id}`
}

async function end (sessionid) {
  if (!sessionid || !sessionid.length) {
    throw new Error('invalid-sessionid')
  }
  const session = await load(sessionid)
  if (!session) {
    throw new Error('invalid-sessionid')
  }
  if (session.ended) {
    throw new Error('invalid-session')
  }
  await global.redisClient.hsetAsync(sessionid, 'ended', Timestamp.now)
}

async function load (sessionid, ignoreDeletedAccounts) {
  if (!sessionid || !sessionid.length) {
    throw new Error('invalid-sessionid')
  }
  const session = await global.redisClient.hgetallAsync(sessionid)
  if (!session) {
    if (!ignoreDeletedAccounts) {
      throw new Error('invalid-sessionid')
    }
    return null
  }
  for (const field in session) {
    try {
      const intValue = parseInt(session[field], 10)
      if (intValue.toString() === session[field]) {
        session[field] = intValue
      }
    } catch (error) {
    }
  }
  if (session.ended) {
    return session
  }
  const account = await global.redisClient.hmgetAsync(session.accountid, ['sessionKey_lastReset', 'sessionKeyNumber'])
  const sessionKeyLastReset = parseInt(account[0], 10)
  const sessionKeyNumber = parseInt(account[1], 10)
  if (session.sessionKeyNumber !== sessionKeyNumber) {
    session.ended = sessionKeyLastReset
  } else if (session.expires <= Timestamp.now) {
    session.ended = session.expires
  }
  return session
}

async function loadMany (sessionids, ignoreDeletedAccounts) {
  if (!sessionids || !sessionids.length) {
    throw new Error('invalid-sessionids')
  }
  const sessions = []
  for (const sessionid of sessionids) {
    const session = await load(sessionid, ignoreDeletedAccounts)
    if (session) {
      sessions.push(session)
    }
  }
  return sessions
}

function hashServerToken (accountid, sessionToken, sessionKey) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  if (!sessionToken || !sessionToken.length) {
    throw new Error('invalid-session.tokenHash')
  }
  if (!sessionKey || !sessionKey.length) {
    throw new Error('invalid-session-key')
  }
  return Hash.fixedSaltHash(`${accountid}:${sessionToken}:${sessionKey}:${global.dashboardSessionKey}`)
}

async function create (accountid, expires) {
  if (!accountid) {
    throw new Error('invalid-accountid')
  }
  if (!expires || expires === 'minutes') {
    expires = Timestamp.date(Timestamp.now + (20 * 60))
  } else if (expires === 'hours') {
    expires = Timestamp.date(Timestamp.now + (8 * 60 * 60))
  } else if (expires === 'days') {
    expires = Timestamp.date(Timestamp.now + (30 * 24 * 60 * 60))
  }
  expires = Timestamp.create(expires)
  const accountFields = await global.redisClient.hmgetAsync(accountid, ['sessionKey', 'sessionKeyNumber'])
  const sessionKey = accountFields[0]
  const sessionKeyNumber = parseInt(accountFields[1], 10)
  const sessionid = await generateID()
  const token = UUID.random(64)
  const tokenHash = hashServerToken(accountid, token, sessionKey)
  const created = Timestamp.now
  const sessionData = [
    'object', 'session',
    `sessionid`, sessionid,
    `accountid`, accountid,
    `tokenHash`, tokenHash,
    `created`, created,
    `expires`, expires,
    `sessionKeyNumber`, sessionKeyNumber
  ]
  await global.redisClient.hsetAsync('map:sessionids', sessionid, accountid)
  await global.redisClient.hmsetAsync(sessionid, sessionData)
  const session = await load(sessionid)
  session.token = token
  return session
}

async function lock (sessionid, url) {
  if (!sessionid || !sessionid.length) {
    throw new Error('invalid-sessionid')
  }
  if (!url || !url.length || !url.startsWith('/')) {
    throw new Error('invalid-url')
  }
  const session = await load(sessionid)
  if (session.lock) {
    throw new Error('invalid-session')
  }
  if (session.unlocked > Timestamp.now) {
    await global.redisClient.hsetAsync(sessionid, `lockURL`, url)
    await global.redisClient.hdelAsync(sessionid, `lockStarted`)
    await global.redisClient.hdelAsync(sessionid, `lockData`)
    return
  }
  await global.redisClient.hdelAsync(sessionid, `unlocked`)
  await global.redisClient.hsetAsync(sessionid, `lock`, Timestamp.now)
  await global.redisClient.hsetAsync(sessionid, `lockURL`, url)
  await global.redisClient.hdelAsync(sessionid, `lockStarted`)
  await global.redisClient.hdelAsync(sessionid, `lockData`)
}

async function unlock (sessionid, longUnlock) {
  if (!sessionid || !sessionid.length) {
    throw new Error('invalid-sessionid')
  }
  const session = await load(sessionid)
  if (!session) {
    throw new Error('invalid-sessionid')
  }
  if (!session.lock) {
    throw new Error('invalid-session')
  }
  if (longUnlock) {
    await global.redisClient.hsetAsync(sessionid, `unlocked`, Timestamp.now + 1200)
  } else {
    await global.redisClient.hsetAsync(sessionid, `unlocked`, 1)
  }
  await global.redisClient.hdelAsync(sessionid, `lock`)
}

async function deleteAccount (accountid) {
  if (!accountid || !accountid.length) {
    throw new Error('invalid-accountid')
  }
  const sessionids = await RedisList.listAll(`account:sessions:${accountid}`)
  if (sessionids && sessionids.length) {
    for (const sessionid of sessionids) {
      await global.redisClient.hdelAsync(`map:sessionids`, sessionid)
      await RedisList.remove('sessions', sessionid)
      await global.redisClient.delAsync(sessionid)
    }
  }
  await global.redisClient.delAsync(`account:sessions:${accountid}`)
}
