/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../index.js')
const TestHelper = require('../test-helper.js')

describe('internal-api/session', () => {
  describe('Session#create()', () => {
    it('should require accountid', async () => {
      let errorMessage
      try {
        await dashboard.Session.create(null, 'days')
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should have 20 minute session if remember is unspecified', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid)
      const now = Math.floor(new Date().getTime() / 1000)
      const minutes = Math.ceil((session.expires - now) / 60)
      assert.strictEqual(minutes, 20)
    })

    it('should have 20 minute session if remember is "minutes"', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid, 'minutes')
      const now = Math.floor(new Date().getTime() / 1000)
      const minutes = Math.ceil((session.expires - now) / 60)
      assert.strictEqual(minutes, 20)
    })

    it('should have 8 hour session if remember is "hours"', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid, 'hours')
      const now = Math.floor(new Date().getTime() / 1000)
      const hours = Math.ceil((session.expires - now) / 60 / 60)
      assert.strictEqual(hours, 8)
    })

    it('should have 30 day session if remember is "days"', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid, 'days')
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((session.expires - now) / 60 / 60 / 24)
      assert.strictEqual(days, 30)
    })
  })

  describe('Session#end()', () => {
    it('should sign out current session', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid, 'days')
      await dashboard.Session.end(session.sessionid)
      const sessionNow = await dashboard.Session.load(session.sessionid)
      assert.notStrictEqual(sessionNow.ended, undefined)
      assert.notStrictEqual(sessionNow.ended, null)
    })

    it('should not end other sessions', async () => {
      const user = await TestHelper.createUser()
      const session1 = await dashboard.Session.create(user.account.accountid, 'days')
      const session2 = await dashboard.Session.create(user.account.accountid, 'hours')
      const session3 = await dashboard.Session.create(user.account.accountid, 'minutes')
      await dashboard.Session.end(session3.sessionid)
      const session1Now = await dashboard.Session.load(session1.sessionid)
      const session2Now = await dashboard.Session.load(session2.sessionid)
      const session3Now = await dashboard.Session.load(session3.sessionid)
      assert.strictEqual(session1Now.ended, undefined)
      assert.strictEqual(session2Now.ended, undefined)
      assert.notStrictEqual(session3Now.ended, undefined)
      assert.notStrictEqual(session3Now.ended, null)
    })
  })

  describe('Session#lock()', () => {
    it('should be unlocked by default', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid, 'days')
      assert.strictEqual(session.unlocked, undefined)
      assert.strictEqual(undefined, session.lockURL)
    })

    it('should require sessionid', async () => {
      let errorMessage
      try {
        await dashboard.Session.lock()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-sessionid')
    })

    it('should require a return url to lock', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid)
      let errorMessage
      try {
        await dashboard.Session.lock(session.sessionid, null)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-url')
    })

    it('should lock session', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid)
      await dashboard.Session.lock(session.sessionid, '/some/url')
      const sessionNow = await dashboard.Session.load(session.sessionid)
      assert.strictEqual(sessionNow.lockURL, '/some/url')
    })
  })

  describe('Session#unlock()', () => {
    it('should unlock session', async () => {
      const user = await TestHelper.createUser()
      const session = await dashboard.Session.create(user.account.accountid, 'days')
      await dashboard.Session.lock(session.sessionid, '/url')
      await dashboard.Session.unlock(session.sessionid)
      const sessionNow = await dashboard.Session.load(session.sessionid)
      assert.strictEqual(sessionNow.lock, undefined)
    })
  })

  describe('Session#deleteAccount', () => {
    it('should require an accountid', async () => {
      let errorMessage
      try {
        await dashboard.Session.deleteAccount()
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should delete the session data', async () => {
      let errorMessage
      const user = await TestHelper.createUser()
      await dashboard.Session.create(user.account.accountid)
      await dashboard.Session.deleteAccount(user.account.accountid)
      try {
        await dashboard.Session.load(user.session.sessionid)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-sessionid')
    })
  })
})
