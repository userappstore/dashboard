const crypto = require('crypto')

module.exports = {
  generateID,
  random,
  v4
}

// this is the 'uuid' module v4 function abbreviated from https://github.com/kelektiv/node-uuid/
const byteToHex = []
for (let i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substring(1)
}

function v4 () {
  const buffer = crypto.randomBytes(16)
  buffer[6] = (buffer[6] & 0x0f) | 0x40
  buffer[8] = (buffer[8] & 0x3f) | 0x80
  let i = 0
  return byteToHex[buffer[i++]] + byteToHex[buffer[i++]] +
    byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + '-' +
    byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + '-' +
    byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + '-' +
    byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + '-' +
    byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + byteToHex[buffer[i++]] + byteToHex[buffer[i++]]
}

// via https://github.com/klughammer/node-randomstring/blob/master/lib/charset.js
function random (length) {
  if (!length) {
    return null
  }
  let string = ''
  const buffer = crypto.randomBytes(length)
  for (const byte of buffer) {
    const characterPosition = byte % global.uuidEncodingCharacters.length
    string += global.uuidEncodingCharacters.charAt(characterPosition)
  }
  return string
}

let seeded = false
// via https://coligo.io/create-url-shortener-with-node-express-mongo/
async function generateID () {
  if (!seeded) {
    seeded = true
    await global.redisClient.setnxAsync(`uuids`, global.uuidSeed)
  }
  const number = await global.redisClient.incrbyAsync(`uuids`, global.uuidIncrement)
  let encoded = ''
  let num = number
  while (num) {
    const remainder = num % global.uuidEncodingCharacters.length
    num = Math.floor(num / global.uuidEncodingCharacters.length)
    encoded = global.uuidEncodingCharacters[remainder].toString() + encoded
  }
  return encoded
}
