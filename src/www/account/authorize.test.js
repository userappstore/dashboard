/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/account/authorize', () => {
  describe('Authorize#BEFORE', () => {
    it('should require locked session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/authorize', 'GET')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-session')
    })
  })

  describe('Authorize#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('Authorize#POST', () => {
    it('should reject missing username', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: '',
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce username length', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: '1',
        password: '123456789123'
      }
      global.minimumUsernameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing password', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user.username,
        password: ''
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce password length', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: '1234567890123',
        password: '1'
      }
      global.minimumPasswordLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject invalid password', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user.username,
        password: 'invalid-password'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should reject different account credentials', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const user2 = await TestHelper.createUser()
      await TestHelper.lockSession(user2)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user2.username,
        password: user2.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-account')
      }
      return req.route.api.post(req, res)
    })

    it('should require impersonating administrator use own credentials', async () => {
      const user = await TestHelper.createUser()
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      await TestHelper.lockSession(administrator)
      const req = TestHelper.createRequest(`/account/authorize`, 'POST')
      req.administratorAccount = administrator.administratorAccount
      req.administratorSession = administrator.administratorSession
      req.account = administrator.account
      req.session = administrator.session
      req.body = {
        username: user.username,
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-account')
      }
      return req.route.api.post(req, res)
    })

    it('should unlock session', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest('/account/authorize', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user.username,
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        assert.strictEqual(req.session.unlocked, 1)
      }
      return req.route.api.post(req, res)
    })

    it('should unlock session for impersonating administrator', async () => {
      const user = await TestHelper.createUser()
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      await TestHelper.lockSession(administrator)
      const req = TestHelper.createRequest(`/account/authorize`, 'POST')
      req.administratorAccount = administrator.administratorAccount
      req.administratorSession = administrator.administratorSession
      req.account = administrator.account
      req.session = administrator.session
      req.body = {
        username: administrator.username,
        password: administrator.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        assert.strictEqual(req.session.unlocked, 1)
      }
      return req.route.api.post(req, res)
    })
  })
})
