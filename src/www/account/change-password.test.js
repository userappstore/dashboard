/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/account/change-password', () => {
  describe('ChangePassword#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/change-password', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('ChangePassword#POST', () => {
    it('should reject missing password', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/change-password', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        password: '',
        confirm: ''
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should reject short password', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/change-password', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        password: '1',
        confirm: '1'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password-length')
      }
      global.minimumPasswordLength = 100
      return req.route.api.post(req, res)
    })

    it('should reject mismatched passwords', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/change-password', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        password: '123',
        confirm: '456'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-confirm')
      }
      return req.route.api.post(req, res)
    })

    it('should apply after authorization', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/change-password', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        password: '123456890',
        confirm: '123456890'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const res = TestHelper.createResponse()
        res.end = async (str) => {
          req.session = await TestHelper.unlockSession(user)
          const res = TestHelper.createResponse()
          res.end = async (str) => {
            const doc = TestHelper.extractDoc(str)
            const messageContainer = doc.getElementById('message-container')
            const message = messageContainer.child[0]
            assert.strictEqual(message.attr.template, 'success')
          }
          return req.route.api.get(req, res)
        }
      }
      return req.route.api.post(req, res)
    })
  })
})
