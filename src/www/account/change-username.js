const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (req.session.lockURL === req.url && req.session.unlocked) {
    req.query = { accountid: req.account.accountid }
    return global.api.user.SetAccountUsername.patch(req)
  }
}

function renderPage (req, res, messageTemplate) {
  if (req.success) {
    messageTemplate = 'success'
  }
  const doc = dashboard.HTML.parse(req.route.html)
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req.body) {
    return renderPage(req, res)
  }
  if (!req.body.username || !req.body.username.length) {
    return renderPage(req, res, 'invalid-username')
  }
  if (global.minimumUsernameLength > req.body.username.length ||
    global.maximumUsernameLength < req.body.username.length) {
    return renderPage(req, res, 'invalid-username-length')
  }
  try {
    req.query = { accountid: req.account.accountid }
    await global.api.user.SetAccountUsername.patch(req)
    if (req.success) {
      return renderPage(req, res, 'success')
    }
    return dashboard.Response.redirect(req, res, `/account/authorize`)
  } catch (error) {
    return renderPage(req, res, 'unknown-error')
  }
}
