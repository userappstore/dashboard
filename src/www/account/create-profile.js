const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (req.session.lockURL === req.url && req.session.unlocked) {
    return global.api.user.CreateProfile.post(req)
  }
}

function renderPage (req, res, messageTemplate) {
  if (req.success) {
    messageTemplate = 'success'
  }
  const doc = dashboard.HTML.parse(req.route.html)
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req || !req.body) {
    return renderPage(req, res)
  }
  if (!req.body.email) {
    return renderPage(req, res, 'invalid-profile-email')
  }
  if (!req.body.firstName || !req.body.firstName.length) {
    return renderPage(req, res, 'invalid-profile-first-name')
  }
  if (global.minimumProfileFirstNameLength > req.body.firstName.length ||
      global.maximumProfileFirstNameLength < req.body.firstName.length) {
    return renderPage(req, res, 'invalid-profile-first-name-length')
  }
  if (!req.body.lastName || !req.body.lastName.length) {
    return renderPage(req, res, 'invalid-profile-last-name')
  }
  if (global.minimumProfileLastNameLength > req.body.lastName.length ||
      global.maximumProfileLastNameLength < req.body.lastName.length) {
    return renderPage(req, res, 'invalid-profile-last-name-length')
  }
  try {
    req.query = req.query || {}
    req.query.accountid = req.account.accountid
    await global.api.user.CreateProfile.post(req)
    if (req.success) {
      return renderPage(req, res, 'success')
    }
    return dashboard.Response.redirect(req, res, `/account/authorize`)
  } catch (error) {
    switch (error.message) {
      case 'invalid-profile-first-name':
      case 'invalid-profile-first-name-length':
      case 'invalid-profile-last-name':
      case 'invalid-profile-last-name-length':
      case 'invalid-profile-email':
        return renderPage(req, res, error.message)
    }
    return renderPage(req, res, 'unknown-error')
  }
}
