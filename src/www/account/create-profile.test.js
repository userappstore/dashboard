/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe(`/account/create-profile`, () => {
  describe('CreateProfile#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/create-profile`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('CreateProfile#POST', () => {
    it('should reject missing first name', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-profile', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: null,
        lastName: 'test',
        email: 'test@test.com'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-profile-first-name')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce first name length', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-profile', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: '1',
        lastName: 'test',
        email: 'test@test.com'
      }
      global.minimumProfileFirstNameLength = 2
      global.maximumProfileFirstNameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-profile-first-name-length')
        // too long
        global.minimumProfileFirstNameLength = 1
        global.maximumProfileFirstNameLength = 1
        const req2 = TestHelper.createRequest('/account/create-profile', 'POST')
        req2.account = user.account
        req2.session = user.session
        req2.body = {
          firstName: '1234567890',
          lastName: 'test',
          email: 'test@test.com'
        }
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const message = doc.getElementById('message-container').child[0]
          assert.strictEqual(message.attr.template, 'invalid-profile-first-name-length')
        }
        return req.route.api.post(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing last name', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-profile', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'First',
        lastName: null,
        email: 'test@test.com'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-profile-last-name')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce last name length', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-profile', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'test',
        lastName: '1',
        email: 'test@test.com'
      }
      global.minimumProfileLastNameLength = 2
      global.maximumProfileLastNameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-profile-last-name-length')
        // too long
        global.minimumProfileLastNameLength = 1
        global.maximumProfileLastNameLength = 1
        const req2 = TestHelper.createRequest('/account/create-profile', 'POST')
        req2.account = user.account
        req2.session = user.session
        req2.body = {
          firstName: 'test',
          lastName: '1234567890',
          email: 'test@test.com'
        }
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const message = doc.getElementById('message-container').child[0]
          assert.strictEqual(message.attr.template, 'invalid-profile-last-name-length')
        }
        return req.route.api.post(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing email', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-profile', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'First',
        lastName: 'Last',
        email: null
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-profile-email')
      }
      global.minimumUsernameLength = 100
      return req.route.api.post(req, res)
    })

    it('should create after authorization', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/create-profile`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: 'Person',
        email: 'email@address.com'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should create and set default after authorization', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/create-profile`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: 'Person',
        email: 'email@address.com',
        default: 'true'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
