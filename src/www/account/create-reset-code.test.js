/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/account/create-reset-code', () => {
  describe('CreateResetCode#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-reset-code', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('CreateResetCode#POST', () => {
    it('should reject missing code', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-reset-code', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        code: ''
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-reset-code')
      }
      return req.route.api.post(req, res)
    })

    it('should reject short code', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-reset-code', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        code: '1'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-reset-code-length')
      }
      global.minimumResetCodeLength = 100
      return req.route.api.post(req, res)
    })

    it('should create after authorization', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-reset-code', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        code: '123456890'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res3 = TestHelper.createResponse()
        res3.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.get(req, res3)
      }
      global.minimumResetCodeLength = 1
      return req.route.api.post(req, res)
    })
  })
})
