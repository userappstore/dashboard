const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (req.session.lockURL === req.url && req.session.unlocked) {
    req.query = { accountid: req.account.accountid }
    return global.api.user.SetAccountDeleted.patch(req)
  }
}

async function renderPage (req, res) {
  if (req.success) {
    return dashboard.Response.redirect(req, res, '/account/delete-account-complete')
  }
  const doc = dashboard.HTML.parse(req.route.html)
  if (global.deleteDelay) {
    const data = {
      numDays: global.deleteDelay || 7
    }
    dashboard.HTML.renderTemplate(doc, data, 'scheduled-delete', 'message-container')
  } else {
    dashboard.HTML.renderTemplate(doc, null, 'instant-delete', 'message-container')
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  try {
    req.query = { accountid: req.account.accountid }
    await global.api.user.SetAccountDeleted.patch(req)
    if (req.success) {
      return renderPage(req, res)
    }
    return dashboard.dashboard.Response.redirect(req, res, '/account/authorize')
  } catch (error) {
    return renderPage(req, res, 'unknown-error')
  }
}
