/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/delete-account', () => {
  describe('DeleteAccount#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/delete-account', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('DeleteAccount#POST', () => {
    it('should apply after authorization', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/delete-account', 'POST')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res3 = TestHelper.createResponse()
        res3.end = async (str) => {
          const account = await dashboard.Account.load(user.account.accountid)
          assert.notStrictEqual(account.deleted, undefined)
          assert.notStrictEqual(account.deleted, null)
        }
        return req.route.api.get(req, res3)
      }
      return req.route.api.post(req, res)
    })
  })
})
