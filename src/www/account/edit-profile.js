const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (!req.query || !req.query.profileid) {
    throw new Error('invalid-profileid')
  }
  if (req.session.lockURL === req.url && req.session.unlocked) {
    return global.api.user.UpdateProfile.patch(req)
  }
  const profile = await dashboard.Profile.load(req.query.profileid)
  req.data = { profile }
}

function renderPage (req, res, messageTemplate) {
  if (req.success) {
    messageTemplate = 'success'
  }
  const doc = dashboard.HTML.parse(req.route.html, req.data.profile, 'profile')
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
  }
  if (req.method === 'GET') {
    for (const field in req.data.profile) {
      const element = doc.getElementById(field)
      if (!element) {
        continue
      }
      element.attr.value = req.data.profile[field]
    }
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req.body) {
    return renderPage(req, res)
  }
  if (!req.body.firstName || !req.body.firstName.length) {
    return renderPage(req, res, 'invalid-profile-first-name')
  }
  if (global.minimumProfileFirstNameLength > req.body.firstName.length ||
      global.maximumProfileFirstNameLength < req.body.firstName.length) {
    return renderPage(req, res, 'invalid-profile-first-name-length')
  }
  if (!req.body.lastName || !req.body.lastName.length) {
    return renderPage(req, res, 'invalid-profile-last-name')
  }
  if (global.minimumProfileLastNameLength > req.body.lastName.length ||
      global.maximumProfileLastNameLength < req.body.lastName.length) {
    return renderPage(req, res, 'invalid-profile-last-name-length')
  }
  if (!req.body.email || !req.body.email.length) {
    return renderPage(req, res, 'invalid-profile-email')
  }
  try {
    await global.api.user.UpdateProfile.patch(req)
    if (req.success) {
      return renderPage(req, res, 'success')
    }
    return dashboard.Response.redirect(req, res, `/account/authorize`)
  } catch (error) {
    switch (error.message) {
      case 'invalid-profile-field':
      case 'invalid-profile-field-length':
        return renderPage(req, res, error.message)
    }
    return renderPage(req, res, 'unknown-error')
  }
}
