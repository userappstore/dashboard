const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  if (req.session.lockURL === req.url && req.session.unlocked) {
    return global.api.user.ResetSessionKey.patch(req)
  }
}

function renderPage (req, res, messageTemplate) {
  if (req.success) {
    return dashboard.Response.redirectToSignIn(req, res)
  }
  const doc = dashboard.HTML.parse(req.route.html)
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  try {
    req.query = { accountid: req.account.accountid }
    await global.api.user.ResetSessionKey.patch(req)
    if (req.success) {
      return dashboard.Response.redirectToSignIn(req, res)
    }
    return dashboard.Response.redirect(req, res, '/account/authorize')
  } catch (error) {
    return renderPage(req, res, 'unknown-error')
  }
}
