/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/end-all-sessions', () => {
  describe('EndAllSessions#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest(`/account/end-all-sessions`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('EndAllSessions#POST', () => {
    it('should generate a new session key', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/end-all-sessions', 'POST')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const account = await dashboard.Account.load(user.account.accountid)
          assert.notStrictEqual(account.sessionKey, user.session.sessionKey)
          assert.notStrictEqual(account.sessionKeyNumber, user.session.sessionKeyNumber)
        }
        await req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should invalidate current session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/end-all-sessions', 'POST')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const session = await dashboard.Session.load(user.session.sessionid)
          assert.notStrictEqual(session.ended, undefined)
          assert.notStrictEqual(session.ended, null)
        }
        return req.route.api.post(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
