const dashboard = require('../../../index.js')

module.exports = {
  get: renderPage
}

async function renderPage(req, res) {
  req.query = req.query || {}
  try {
    req.query.sessionid = req.session.sessionid
    req.session = await global.api.user.ResetSessionUnlocked.patch(req)
  } catch (error) {
    return dashboard.Response.throw500(req, res)
  }
  return dashboard.Response.redirect(req, res, req.query.returnURL || '/home')
}
