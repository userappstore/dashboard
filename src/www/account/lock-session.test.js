/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/lock-session', () => {
  describe('LockSession#GET', () => {
    it('should reject invalid session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/lock-session`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async () => {
        assert.strictEqual(res.statusCode, 500)
      }
      return req.route.api.get(req, res)
    })

    it('should remove unlocked status', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      await TestHelper.unlockSession(user)
      const req = TestHelper.createRequest(`/account/lock-session`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const session = await dashboard.Session.load(user.session.sessionid)
        assert.strictEqual(session.unlocked, undefined)
      }
      return req.route.api.get(req, res)
    })
  })
})
