const dashboard = require('../../../index.js')

module.exports = {
  get: renderPage,
  post: submitForm
}

function renderPage (req, res, messageTemplate) {
  const doc = dashboard.HTML.parse(req.route.html)
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
  }
  const requirements = {
    object: 'requirements',
    minimumUsernameLength: global.minimumUsernameLength,
    minimumPasswordLength: global.minimumPasswordLength
  }
  dashboard.HTML.renderTemplate(doc, requirements, 'input-fields', 'input-container')
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req || !req.body) {
    return renderPage(req, res, 'invalid-username')
  }
  if (!req.body.username || !req.body.username.length) {
    return renderPage(req, res, 'invalid-username')
  }
  if (!req.body.password || !req.body.password.length) {
    return renderPage(req, res, 'invalid-password')
  }
  if (global.minimumUsernameLength > req.body.username.length) {
    return renderPage(req, res, 'invalid-username-length')
  }
  if (global.minimumPasswordLength > req.body.password.length) {
    return renderPage(req, res, 'invalid-password-length')
  }
  if (req.body.confirm !== req.body.password) {
    return renderPage(req, res, 'invalid-confirm')
  }
  try {
    req.account = await global.api.user.CreateAccount.post(req)
  } catch (error) {
    switch (error) {
      case 'invalid-username':
        return renderPage(req, res, 'invalid-username-error')
      case 'invalid-password':
        return renderPage(req, res, 'invalid-username-password')
      default:
        return renderPage(req, res, 'unknown-error')
    }
  }
  req.route = global.sitemap['/account/signin']
  return req.route.api.post(req, res)
}
