/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/register', () => {
  describe('Register#GET', () => {
    it('should present the form', async () => {
      const req = TestHelper.createRequest('/account/register', 'GET')
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('Register#POST', () => {
    it('should reject missing username', async () => {
      const req = TestHelper.createRequest('/account/register', 'POST')
      req.body = {
        username: '',
        password: 'new-password',
        confirm: 'new-password'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce username length', async () => {
      const req = TestHelper.createRequest('/account/register', 'POST')
      req.body = {
        username: '1',
        password: 'new-password',
        confirm: 'new-password'
      }
      global.minimumUsernameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing password', async () => {
      const req = TestHelper.createRequest('/account/register', 'POST')
      req.body = {
        username: 'new-username',
        password: '',
        confirm: 'new-password'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce password length', async () => {
      const req = TestHelper.createRequest('/account/register', 'POST')
      req.body = {
        username: 'new-username',
        password: '1',
        confirm: '1'
      }
      global.minimumPasswordLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject invalid confirm', async () => {
      const req = TestHelper.createRequest('/account/register', 'POST')
      req.body = {
        username: '1234567890123',
        password: '1234567890123',
        confirm: '123'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-confirm')
      }
      return req.route.api.post(req, res)
    })

    it('should create account and 20-minute session', async () => {
      const req = TestHelper.createRequest('/account/register', 'POST')
      req.body = {
        username: 'new-user-' + new Date().getTime(),
        password: 'a-user-password',
        confirm: 'a-user-password'
      }
      const res = TestHelper.createResponse()
      res.end = async () => {
        const session = await dashboard.Session.load(req.session.sessionid)
        const hours = Math.floor((session.expires - dashboard.Timestamp.now) / 60 / 60)
        assert.strictEqual(hours, 0)
      }
      return req.route.api.post(req, res)
    })
  })
})
