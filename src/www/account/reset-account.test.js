/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/reset-account', () => {
  describe('ResetAccount#GET', () => {
    it('should present the form', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'GET')
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('ResetAccount#POST', () => {
    it('should reject missing username', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: null,
        password: 'new-password',
        confirm: 'new-password',
        code: 'reset-code'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce username length', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: '1',
        password: 'new-password',
        confirm: 'new-password',
        code: 'reset-code'
      }
      global.minimumUsernameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing reset code', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: 'username',
        password: 'new-password',
        confirm: 'new-password',
        code: null
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-reset-code')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce reset code length', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: 'username',
        password: 'new-password',
        confirm: 'new-password',
        code: '1'
      }
      global.minimumResetCodeLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-reset-code-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing password', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: 'username',
        password: '',
        confirm: 'new-password',
        code: 'reset-code'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce password length', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: 'username',
        password: '1',
        confirm: '1',
        code: 'reset-code'
      }
      global.minimumPasswordLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password-length')
      }
      return req.route.api.post(req, res)
    })

    it('should require confirm', async () => {
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: 'username',
        password: 'new-password',
        confirm: '',
        code: 'reset-code'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-confirm')
      }
      return req.route.api.post(req, res)
    })

    it('should not reset deleted account', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      global.deleteDelay = -1
      await TestHelper.setDeleted(user)
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: user.username,
        password: 'my-new-password',
        confirm: 'my-new-password',
        code: user.resetCode.code
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-account')
      }
      return req.route.api.post(req, res)
    })

    it('should reset session key', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: user.username,
        password: 'my-new-password',
        confirm: 'my-new-password',
        code: user.resetCode.code
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const account = await dashboard.Account.load(user.account.accountid)
        assert.notStrictEqual(account.sessionKey_lastReset, undefined)
        assert.notStrictEqual(account.sessionKey_lastReset, null)
      }
      return req.route.api.post(req, res)
    })

    it('should reset code last used', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: user.username,
        password: 'my-new-password',
        confirm: 'my-new-password',
        code: user.resetCode.code
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const account = await dashboard.Account.load(user.account.accountid)
        assert.notStrictEqual(account.resetCode_lastUsed, undefined)
        assert.notStrictEqual(account.resetCode_lastUsed, null)
      }
      return req.route.api.post(req, res)
    })

    it('should sign in', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: user.username,
        password: 'my-new-password',
        confirm: 'my-new-password',
        code: user.resetCode.code
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        assert.notStrictEqual(res.headers['set-cookie'], undefined)
        assert.notStrictEqual(res.headers['set-cookie'], null)
      }
      return req.route.api.post(req, res)
    })
  })
})
