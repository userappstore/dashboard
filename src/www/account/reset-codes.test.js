/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/reset-codes', () => {
  describe('ResetCodes#BEFORE', () => {
    it('should bind reset codes to req', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest('/account/reset-codes', 'GET')
      req.account = user.account
      req.session = user.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.resetCodes.length, 1)
      assert.strictEqual(req.data.resetCodes[0].accountid, user.account.accountid)
    })
  })

  describe('ResetCodes#GET', () => {
    it('should show never created', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/reset-codes', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const lastCreated = doc.getElementById('last-created-1')
        assert.strictEqual(lastCreated.child.length, 1)
      }
      return req.route.api.get(req, res)
    })

    it('should update created date', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/create-reset-code', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        code: 'code-' + new Date().getTime() + '-' + Math.ceil(Math.random() * 1000)
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        await req.route.api.before(req)
        const req2 = TestHelper.createRequest('/account/reset-codes', 'GET')
        req2.account = await dashboard.Account.load(user.account.accountid)
        req2.session = await dashboard.Session.load(user.session.sessionid)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const lastCreated = doc.getElementById('last-created-1')
          const date = dashboard.Format.date(new Date())
          assert.strictEqual(lastCreated.parentNode.toString().indexOf(date) > -1, true)
        }
        return req2.route.api.get(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should show never deleted', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/reset-codes', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const lastDeleted = doc.getElementById('last-deleted-2')
        assert.strictEqual(lastDeleted.child.length, 1)
      }
      return req.route.api.get(req, res)
    })

    it('should update deleted date', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest(`/account/delete-reset-code?codeid=${user.resetCode.codeid}`, 'POST')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        await req.route.api.before(req)
        const req2 = TestHelper.createRequest('/account/reset-codes', 'GET')
        req2.account = await dashboard.Account.load(user.account.accountid)
        req2.session = await dashboard.Session.load(user.session.sessionid)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const lastDeleted = doc.getElementById('last-deleted-2')
          const date = dashboard.Format.date(new Date())
          assert.strictEqual(lastDeleted.parentNode.toString().indexOf(date) > -1, true)
        }
        await req2.route.api.get(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should show never used', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/reset-codes', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const lastUsed = doc.getElementById('last-used-3')
        assert.strictEqual(lastUsed.child.length, 1)
      }
      return req.route.api.get(req, res)
    })

    it('should update used date', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest('/account/reset-account', 'POST')
      req.body = {
        username: user.username,
        password: 'my-new-password',
        confirm: 'my-new-password',
        code: user.resetCode.code
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        user.password = 'my-new-password'
        user.session = await TestHelper.createSession(user)
        const req2 = TestHelper.createRequest('/account/reset-codes', 'GET')
        req2.account = await dashboard.Account.load(user.account.accountid)
        req2.session = user.session
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const lastUsed = doc.getElementById('last-used-3')
          const date = dashboard.Format.date(new Date())
          assert.strictEqual(lastUsed.parentNode.toString().indexOf(date) > -1, true)
        }
        return req2.route.api.get(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should limit reset codes to one page', async () => {
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createResetCode(user)
      }
      const req = TestHelper.createRequest('/account/reset-codes', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('reset-codes-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createResetCode(user)
      }
      const req = TestHelper.createRequest('/account/reset-codes', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('reset-codes-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const user = await TestHelper.createUser()
      const codes = [ user.resetCode ]
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createResetCode(user)
        codes.unshift(user.resetCode)
      }
      const req = TestHelper.createRequest(`/account/reset-codes?offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        for (let i = 0, len = global.pageSize; i < len; i++) {
          assert.strictEqual(doc.getElementById(codes[offset + i].codeid).tag, 'tr')
        }
      }
      return req.route.api.get(req, res)
    })
  })
})
