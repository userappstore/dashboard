/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/restore-account', () => {
  describe('RestoreAccount#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/restore-account', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('RestoreAccount#POST', () => {
    it('should reject missing username', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/restore-account', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: '',
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce username length', async () => {
      const req = TestHelper.createRequest('/account/restore-account', 'POST')
      req.body = {
        username: '1',
        password: '123456789123'
      }
      global.minimumUsernameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing password', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/restore-account', 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user.username,
        password: ''
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce password length', async () => {
      const req = TestHelper.createRequest('/account/restore-account', 'POST')
      req.body = {
        username: '1234567890123',
        password: '1'
      }
      global.minimumPasswordLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject invalid password', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/restore-account', 'POST')
      req.body = {
        username: user.username,
        password: 'invalid-password'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should reject if after deletion date', async () => {
      global.deleteDelay = -1
      const user = await TestHelper.createUser()
      await TestHelper.setDeleted(user)
      const req = TestHelper.createRequest('/account/restore-account', 'POST')
      req.body = {
        username: user.username,
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-account')
      }
      return req.route.api.post(req, res)
    })

    it('should unset account deleted', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/delete-account', 'POST')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        await req.route.api.before(req)
        const req2 = TestHelper.createRequest('/account/restore-account', 'POST')
        req2.body = {
          username: user.username,
          password: user.password
        }
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const account = await dashboard.Account.load(user.account.accountid)
          assert.strictEqual(account.deleted, undefined)
        }
        return req2.route.api.post(req2, res2)
      }
      global.deleteDelay = 1
      return req.route.api.post(req, res)
    })
  })
})
