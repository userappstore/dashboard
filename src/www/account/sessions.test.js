/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/sessions', () => {
  describe('Sessions#BEFORE', () => {
    it('should bind sessions to req', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/sessions`, 'GET')
      req.account = user.account
      req.session = user.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.sessions.length, 1)
      assert.strictEqual(req.data.sessions[0].sessionid, user.session.sessionid)
    })
  })

  describe('Sessions#GET', () => {
    it('should show never reset date', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/sessions', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const lastReset = doc.getElementById('last-reset-1')
        assert.strictEqual(lastReset.child.length, 1)
      }
      return req.route.api.get(req, res)
    })

    it('should update last reset date', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/account/end-all-sessions?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        await req.route.api.before(req)
        await TestHelper.createSession(user)
        const req2 = TestHelper.createRequest('/account/sessions', 'GET')
        req2.account = await dashboard.Account.load(user.account.accountid)
        req2.session = user.session
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const lastReset = doc.getElementById('last-reset-1')
          const date = dashboard.Format.date(new Date())
          assert.strictEqual(lastReset.parentNode.toString().indexOf(date) > -1, true)
        }
        return req2.route.api.get(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should update last signed in date', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const req2 = TestHelper.createRequest('/account/sessions', 'GET')
        req2.account = await dashboard.Account.load(user.account.accountid)
        req2.session = await dashboard.Session.load(user.session.sessionid)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const lastSignin = doc.getElementById('last-signin-2')
          const date = dashboard.Format.date(new Date())
          assert.strictEqual(lastSignin.parentNode.toString().indexOf(date) > -1, true)
        }
        return req2.route.api.get(req2, res2)
      }
      return req.route.api.post(req, res)
    })

    it('should exclude ended sessions', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signout', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const req2 = TestHelper.createRequest('/account/signin', 'POST')
        req2.body = {
          username: user.username,
          password: user.password
        }
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const sessionRow = doc.getElementById(`${req.session.sessionid}`)
          assert.strictEqual(undefined, sessionRow)
        }
        return req2.route.api.get(req2, res2)
      }
      return req.route.api.get(req, res)
    })

    it('should limit sessions to one page', async () => {
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createSession(user)
      }
      const req = TestHelper.createRequest('/account/sessions', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('sessions-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createSession(user)
      }
      const req = TestHelper.createRequest('/account/sessions', 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('sessions-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const user = await TestHelper.createUser()
      const sessions = [ user.session ]
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createSession(user)
        sessions.unshift(user.session)
      }
      const req = TestHelper.createRequest(`/account/sessions?offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        for (let i = 0, len = global.pageSize; i < len; i++) {
          assert.strictEqual(doc.getElementById(sessions[offset + i].sessionid).tag, 'tr')
        }
      }
      return req.route.api.get(req, res)
    })
  })
})
