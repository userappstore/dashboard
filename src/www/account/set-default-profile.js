const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage,
  post: submitForm
}

async function beforeRequest (req) {
  req.query = req.query || {}
  req.query.accountid = req.account.accountid
  if (req.session.lockURL === req.url && req.session.unlocked) {
    return global.api.user.SetAccountProfile.patch(req)
  }
  let total = await global.api.user.ProfilesCount.get(req)
  req.query.offset = 0
  let profiles = []
  while (total > 0) {
    const more = await global.api.user.Profiles.get(req)
    total -= global.pageSize
    req.query.offset += global.pageSize
    if (more && more.length) {
      profiles = profiles.concat(more)
    }
  }
  req.query.profileid = req.body && req.body.profileid ? req.body.profileid : req.query.profileid || req.account.profileid
  const profile = await global.api.user.Profile.get(req)
  req.data = { profile, profiles }
}

function renderPage (req, res, messageTemplate) {
  if (req.success) {
    messageTemplate = 'success'
  }
  const doc = dashboard.HTML.parse(req.route.html, req.data.profile, 'profile')
  if (messageTemplate) {
    dashboard.HTML.renderTemplate(doc, null, messageTemplate, 'message-container')
    if (messageTemplate === 'success') {
      const submitForm = doc.getElementById('submit-form')
      submitForm.parentNode.removeChild(submitForm)
      return dashboard.Response.end(req, res, doc)
    }
  }
  if (req.data.profiles && req.data.profiles.length) {
    dashboard.HTML.renderList(doc, req.data.profiles, 'profile-option', 'profileid')
    dashboard.HTML.setSelectedOptionByValue(doc, 'profileid', req.query.profileid)
  }
  return dashboard.Response.end(req, res, doc)
}

async function submitForm (req, res) {
  if (!req.body || !req.body.profileid) {
    return renderPage(req, res)
  }
  if (req.account.profileid === req.body.profileid) {
    return renderPage(req, res, 'default-profile')
  }
  try {
    req.query = { accountid: req.account.accountid }
    await global.api.user.SetAccountProfile.patch(req)
    if (req.success) {
      return renderPage(req, res, 'success')
    }
    return dashboard.Response.redirect(req, res, '/account/authorize')
  } catch (error) {
    if (error.message === 'invalid-profile') {
      return renderPage(req, res, 'default-profile')
    }
    return renderPage(req, res, 'unknown-error')
  }
}
