/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/account/set-default-profile', () => {
  describe('SetDefaultProfile#BEFORE', () => {
    it('should bind posted profile to req', async () => {
      const user = await TestHelper.createUser()
      const profile1 = user.profile
      await TestHelper.createProfile(user)
      const req = TestHelper.createRequest(`/account/set-default-profile`, 'GET')
      req.account = user.account
      req.session = user.session
      req.body = {
        profileid: profile1.profileid
      }
      await req.route.api.before(req)
      assert.strictEqual(req.data.profile.profileid, profile1.profileid)
    })

    it('should bind all profiles to req', async () => {
      global.pageSize = 1
      const user = await TestHelper.createUser()
      const profile1 = user.profile
      const profile2 = await TestHelper.createProfile(user)
      const profile3 = await TestHelper.createProfile(user)
      const req = TestHelper.createRequest(`/account/set-default-profile`, 'GET')
      req.account = user.account
      req.session = user.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.profiles.length, 3)
      assert.strictEqual(req.data.profiles[0].profileid, profile3.profileid)
      assert.strictEqual(req.data.profiles[1].profileid, profile2.profileid)
      assert.strictEqual(req.data.profiles[2].profileid, profile1.profileid)
    })
  })

  describe('SetDefaultProfile#GET', () => {
    it('should present the form', async () => {
      const user = await TestHelper.createUser()
      const profile1 = user.profile
      await TestHelper.createProfile(user)
      const req = TestHelper.createRequest(`/account/set-default-profile`, 'GET')
      req.account = user.account
      req.session = user.session
      req.body = {
        profileid: profile1.profileid
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })

    it('should present the profile table', async () => {
      const user = await TestHelper.createUser()
      const profile1 = user.profile
      await TestHelper.createProfile(user)
      const req = TestHelper.createRequest(`/account/set-default-profile`, 'GET')
      req.account = user.account
      req.session = user.session
      req.body = {
        profileid: profile1.profileid
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('profiles-table')
        const row = table.getElementById(profile1.profileid)
        assert.strictEqual(row.tag, 'tr')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('SetDefaultProfile#POST', () => {
    it('should set the authorized profile as default', async () => {
      const user = await TestHelper.createUser()
      const profile1 = user.profile
      await TestHelper.createProfile(user)
      const req = TestHelper.createRequest(`/account/set-default-profile`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        profileid: profile1.profileid
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(user)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const doc = TestHelper.extractDoc(str)
          const messageContainer = doc.getElementById('message-container')
          const message = messageContainer.child[0]
          assert.strictEqual(message.attr.template, 'success')
        }
        return req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
