/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/account/signin', () => {
  describe('Signin#GET', () => {
    it('should present the form', async () => {
      const req = TestHelper.createRequest('/account/signin', 'GET')
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        assert.strictEqual(doc.getElementById('submit-form').tag, 'form')
        assert.strictEqual(doc.getElementById('submit-button').tag, 'button')
      }
      return req.route.api.get(req, res)
    })
  })

  describe('Signin#POST', () => {
    it('should reject missing username', async () => {
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: '',
        password: 'password'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce username length', async () => {
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: '1',
        password: '123456789123'
      }
      global.minimumUsernameLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-username-length')
      }
      return req.route.api.post(req, res)
    })

    it('should reject missing password', async () => {
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: 'asdfasdf',
        password: ''
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should reject invalid password', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: 'invalid-password'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password')
      }
      return req.route.api.post(req, res)
    })

    it('should enforce password length', async () => {
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: '1234567890123',
        password: '1'
      }
      global.minimumPasswordLength = 100
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const message = doc.getElementById('message-container').child[0]
        assert.strictEqual(message.attr.template, 'invalid-password-length')
      }
      return req.route.api.post(req, res)
    })

    it('should create session expiring in 20 minutes as default', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async () => {
        const session = await dashboard.Session.load(req.session.sessionid)
        const minutes = Math.ceil((session.expires - dashboard.Timestamp.now) / 60)
        assert.strictEqual(minutes, 20)
      }
      return req.route.api.post(req, res)
    })

    it('should create session expiring in 20 minutes', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: user.password,
        remember: 'minutes'
      }
      const res = TestHelper.createResponse()
      res.end = async () => {
        const session = await dashboard.Session.load(req.session.sessionid)
        const minutes = Math.ceil((session.expires - dashboard.Timestamp.now) / 60)
        assert.strictEqual(minutes, 20)
      }
      return req.route.api.post(req, res)
    })

    it('should create session expiring in 8 hours', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: user.password,
        remember: 'hours'
      }
      const res = TestHelper.createResponse()
      res.end = async () => {
        const session = await dashboard.Session.load(req.session.sessionid)
        const hours = Math.ceil((session.expires - dashboard.Timestamp.now) / 60 / 60)
        assert.strictEqual(hours, 8)
      }
      return req.route.api.post(req, res)
    })

    it('should create session expiring in 30 days', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: user.password,
        remember: 'days'
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const session = await dashboard.Session.load(req.session.sessionid)
        const days = Math.ceil((session.expires - dashboard.Timestamp.now) / 60 / 60 / 24)
        assert.strictEqual(days, 30)
      }
      return req.route.api.post(req, res)
    })
  })
})
