/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/account/signout', () => {
  describe('Signout#GET', () => {
    it('should end the session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/account/signin', 'POST')
      req.body = {
        username: user.username,
        password: user.password
      }
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const req2 = TestHelper.createRequest('/account/signout', 'GET')
        req2.account = user.account
        req2.session = user.session
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          assert.notStrictEqual(res.headers['set-cookie'], res2.headers['set-cookie'])
        }
        return req2.route.api.get(req2, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
