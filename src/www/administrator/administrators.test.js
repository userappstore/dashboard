/* eslint-env mocha */
const TestHelper = require('../../../test-helper.js')
const assert = require('assert')

describe('/administrator/administrators', () => {
  describe('Administrators#BEFORE', () => {
    it('should bind administrators to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      const administrator2 = await TestHelper.createAdministrator()
      const req = TestHelper.createRequest('/administrator/administrators', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.administrators.length, global.pageSize)
      assert.strictEqual(req.data.administrators[0].accountid, administrator2.account.accountid)
      assert.strictEqual(req.data.administrators[1].accountid, administrator.account.accountid)
    })
  })

  describe('Administrators#GET', () => {
    it('should present the administrators table', async () => {
      const administrator = await TestHelper.createAdministrator()
      const req = TestHelper.createRequest('/administrator/administrators', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const row = doc.getElementById(administrator.account.accountid)
        assert.strictEqual(row.tag, 'tr')
      }
      return req.route.api.get(req, res)
    })

    it('should limit accounts to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createAdministrator()
      }
      const req = TestHelper.createRequest('/administrator/administrators', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('administrators-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createAdministrator()
      }
      const req = TestHelper.createRequest('/administrator/administrators', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('administrators-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const administrators = [ administrator.account ]
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        const user = await TestHelper.createAdministrator()
        administrators.unshift(user.account)
      }
      const req = TestHelper.createRequest(`/administrator/administrators?offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        for (let i = 0, len = global.pageSize; i < len; i++) {
          assert.strictEqual(doc.getElementById(administrators[offset + i].accountid).tag, 'tr')
        }
      }
      return req.route.api.get(req, res)
    })
  })
})
