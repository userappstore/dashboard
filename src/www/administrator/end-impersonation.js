const dashboard = require('../../../index.js')

module.exports = {
  before: beforeRequest,
  get: renderPage
}

async function beforeRequest (req) {
  if (req.administratorAccount.accountid === req.account.accountid) {
    throw new Error('invalid-session')
  }
}

async function renderPage (req, res) {
  try {
    req.query = { sessionid: req.administratorSession.sessionid }
    await global.api.administrator.ResetSessionImpersonate.patch(req)
  } catch (error) {
    return dashboard.Response.throw500(req, res)
  }
  const returnURL = `/administrator/account?accountid=${req.session.accountid}`
  dashboard.Response.redirect(req, res, returnURL)
}
