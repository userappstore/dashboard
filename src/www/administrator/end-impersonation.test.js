/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/administrator/end-impersonation', () => {
  describe('EndImpersonation#BEFORE', () => {
    it('should reject invalid session', async () => {
      const administrator = await TestHelper.createAdministrator()
      const req = TestHelper.createRequest(`/administrator/end-impersonation`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      let errorMessage
      try {
        await req.route.api.before(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-session')
    })

    it('should remove impersonate status', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      const req = TestHelper.createRequest(`/administrator/end-impersonation`, 'GET')
      req.administratorAccount = administrator.administratorAccount
      req.administratorSession = administrator.administratorSession
      req.account = administrator.account
      req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const sessionNow = await dashboard.Session.load(administrator.administratorSession.sessionid)
        assert.strictEqual(undefined, sessionNow.impersonate, null)
        const userSessionNow = await dashboard.Session.load(administrator.session.sessionid)
        assert.notStrictEqual(userSessionNow.ended, undefined)
        assert.notStrictEqual(userSessionNow.ended, null)
      }
      return req.route.api.get(req, res)
    })
  })
})
