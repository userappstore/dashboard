/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../index.js')
const TestHelper = require('../../../test-helper.js')

describe('/administrator/impersonate-account', () => {
  describe('ImpersonateAccount#GET', () => {
    it('should set impersonate status', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/administrator/impersonate-account?accountid=${user.account.accountid}`, 'POST')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        req.session = await TestHelper.unlockSession(administrator)
        const res2 = TestHelper.createResponse()
        res2.end = async (str) => {
          const session = await dashboard.Session.load(administrator.session.sessionid)
          const impersonate = await dashboard.Session.load(session.impersonate)
          assert.strictEqual(impersonate.accountid, user.account.accountid)
        }
        return req.route.api.get(req, res2)
      }
      return req.route.api.post(req, res)
    })
  })
})
