/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/administrator/reset-code', () => {
  describe('ResetCode#BEFORE', () => {
    it('should bind reset code to req', async () => {
      const user = await TestHelper.createUser()
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest(`/administrator/reset-code?codeid=${user.resetCode.codeid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.resetCode.codeid, user.resetCode.codeid)
    })
  })

  describe('ResetCode#GET', () => {
    it('should present the reset code table', async () => {
      const user = await TestHelper.createUser()
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest(`/administrator/reset-code?codeid=${user.resetCode.codeid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('reset-codes-table')
        const tbody = table.getElementById(user.resetCode.codeid)
        assert.strictEqual(tbody.tag, 'tbody')
      }
      return req.route.api.get(req, res)
    })
  })
})
