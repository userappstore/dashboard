/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/administrator/session', () => {
  describe('Session#BEFORE', () => {
    it('should bind session to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/administrator/session?sessionid=${user.session.sessionid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.session.accountid, user.account.accountid)
    })
  })

  describe('Session#GET', () => {
    it('should present the session table', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/administrator/session?sessionid=${user.session.sessionid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('sessions-table')
        const tbody = table.getElementById(user.session.sessionid)
        assert.strictEqual(tbody.tag, 'tbody')
      }
      return req.route.api.get(req, res)
    })
  })
})
