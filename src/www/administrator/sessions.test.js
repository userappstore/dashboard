/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../test-helper.js')

describe('/administrator/sessions', () => {
  describe('Sessions#BEFORE', () => {
    it('should bind sessions to req', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/administrator/sessions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.before(req)
      assert.strictEqual(req.data.sessions.length, global.pageSize)
      assert.strictEqual(req.data.sessions[0].accountid, user.account.accountid)
      assert.strictEqual(req.data.sessions[1].accountid, administrator.account.accountid)
    })
  })

  describe('Sessions#GET', () => {
    it('should present the sessions table', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest('/administrator/sessions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('sessions-table')
        const tableString = table.toString()
        assert.strictEqual(tableString.indexOf(administrator.session.accountid) > -1, true)
        assert.strictEqual(tableString.indexOf(user.session.accountid) > -1, true)
      }
      return req.route.api.get(req, res)
    })

    it('should limit sessions to one page', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createSession(user)
      }
      const req = TestHelper.createRequest('/administrator/sessions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('sessions-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createSession(user)
      }
      const req = TestHelper.createRequest('/administrator/sessions', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        const table = doc.getElementById('sessions-table')
        const rows = table.getElementsByTagName('tr')
        assert.strictEqual(rows.length, global.pageSize + 1)
      }
      return req.route.api.get(req, res)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const sessions = [ administrator.session ]
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        const user = await TestHelper.createUser()
        sessions.unshift(user.session)
      }
      const req = TestHelper.createRequest(`/administrator/sessions?offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const res = TestHelper.createResponse()
      res.end = async (str) => {
        const doc = TestHelper.extractDoc(str)
        for (let i = 0, len = global.pageSize; i < len; i++) {
          assert.strictEqual(doc.getElementById(sessions[offset + i].sessionid).tag, 'tr')
        }
      }
      return req.route.api.get(req, res)
    })
  })
})
