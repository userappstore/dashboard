/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe('/api/administrator/account-profiles-count', () => {
  describe('AccountProfilesCount#GET', () => {
    it('should count account\'s profiles', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.createUser()
      await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/account-profiles-count?accountid=${user.account.accountid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const result = await req.route.api.get(req)
      assert.strictEqual(result, 1)
    })
  })
})
