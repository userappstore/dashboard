const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    let profileids
    if (req.query.all) {
      profileids = await dashboard.RedisList.listAll(`account:profiles:${req.query.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      profileids = await dashboard.RedisList.list(`account:profiles:${req.query.accountid}`, offset)
    }
    if (!profileids || !profileids.length) {
      return null
    }
    return dashboard.Profile.loadMany(profileids)
  }
}
