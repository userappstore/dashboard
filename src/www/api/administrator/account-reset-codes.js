const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    let codeids
    if (req.query.all) {
      codeids = await dashboard.RedisList.listAll(`account:resetCodes:${req.query.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      codeids = await dashboard.RedisList.list(`account:resetCodes:${req.query.accountid}`, offset)
    }
    if (!codeids || !codeids.length) {
      return null
    }
    const resetCodes = await dashboard.ResetCode.loadMany(codeids)
    for (const code of resetCodes) {
      delete (code.codeHash)
    }
    return resetCodes
  }
}
