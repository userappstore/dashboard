const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    delete (account.sessionKey)
    delete (account.usernameHash)
    delete (account.passwordHash)
    return account
  }
}
