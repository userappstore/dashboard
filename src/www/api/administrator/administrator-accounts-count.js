const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    return dashboard.RedisList.count('administrator:accounts')
  }
}
