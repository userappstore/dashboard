/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe('/api/administrator/administrator-accounts-count', () => {
  describe('AdministratorAccountsCount#GET', () => {
    it('should count all administrators\' accounts', async () => {
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.createAdministrator()
      const req = TestHelper.createRequest('/api/administrator/administrator-accounts-count', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const result = await req.route.api.get(req)
      assert.strictEqual(result, 2)
    })
  })
})
