const dashboard = require('../../../../index.js')

module.exports = {
  /**
   * Returns a list of administrators bound to profile information
   */
  get: async (req) => {
    req.query = req.query || {}
    let accountids
    if (req.query.all) {
      accountids = await dashboard.RedisList.listAll(`administrator:accounts`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      accountids = await dashboard.RedisList.list(`administrator:accounts`, offset)
    }
    if (!accountids || !accountids.length) {
      return null
    }
    const accounts = await dashboard.Account.loadMany(accountids)
    for (const account of accounts) {
      delete (account.sessionKey)
      delete (account.usernameHash)
      delete (account.passwordHash)
    }
    return accounts
  }
}
