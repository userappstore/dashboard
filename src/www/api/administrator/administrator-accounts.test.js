/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe('/api/administrator/administrator-accounts', () => {
  describe('AdministratorAccounts#GET', () => {
    it('should return administrators\' accounts', async () => {
      const administrator = await TestHelper.createAdministrator()
      const administrator2 = await TestHelper.createAdministrator()
      const req = TestHelper.createRequest('/api/administrator/administrator-accounts', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const administrators = await req.route.api.get(req)
      assert.strictEqual(administrators.length, global.pageSize)
      assert.strictEqual(administrators[0].accountid, administrator2.account.accountid)
      assert.strictEqual(administrators[1].accountid, administrator.account.accountid)
    })

    it('should redact username, password, session key', async () => {
      const administrator = await TestHelper.createAdministrator()
      const req = TestHelper.createRequest('/api/administrator/administrator-accounts', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const administrators = await req.route.api.get(req)
      assert.strictEqual(administrators[0].accountid, administrator.account.accountid)
      assert.strictEqual(undefined, administrators[0].username)
      assert.strictEqual(undefined, administrators[0].password)
      assert.strictEqual(undefined, administrators[0].sessionKey)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const administrator = await TestHelper.createAdministrator()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createAdministrator()
      }
      const req = TestHelper.createRequest('/api/administrator/administrator-accounts', 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const accountsNow = await req.route.api.get(req)
      assert.strictEqual(accountsNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const administrator = await TestHelper.createAdministrator()
      const accounts = [ administrator.account ]
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        const administrator = await TestHelper.createAdministrator()
        accounts.unshift(administrator.account)
      }
      const req = TestHelper.createRequest(`/api/administrator/administrator-accounts?offset=${offset}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const accountsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(accountsNow[i].accountid, accounts[offset + i].accountid)
      }
    })

    it('should return all records', async () => {
      const administrator = await TestHelper.createAdministrator()
      const accounts = [administrator.account]
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        const administrator = await TestHelper.createAdministrator()
        accounts.unshift(administrator.account)
      }
      const req = TestHelper.createRequest(`/api/administrator/administrator-accounts?all=true`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const accountsNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(accountsNow[i].accountid, accounts[i].accountid)
      }
    })
  })
})
