const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.body && req.body.codeHash) {
      return
    }
    if (!req.body || !req.body.code || !req.body.code.length) {
      throw new Error('invalid-reset-code')
    }
    if (global.minimumResetCodeLength > req.body.code.length ||
      global.maximumResetCodeLength < req.body.code.length) {
      throw new Error('invalid-reset-code-length')
    }
    req.body.codeHash = dashboard.Hash.fixedSaltHash(req.body.code)
    delete (req.body.code)
  },
  post: async (req) => {
    const resetCode = await dashboard.ResetCode.create(req.query.accountid, req.body.codeHash)
    await dashboard.RedisObject.setProperties(resetCode.codeid, { ip: req.ip, userAgent: req.userAgent })
    req.success = true
    return resetCode
  }
}
