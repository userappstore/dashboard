const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    req.data = { account }
  },
  delete: async (req) => {
    await dashboard.Account.deleteAccount(req.query.accountid)
    await dashboard.RedisList.remove('accounts', req.query.accountid)
    if (req.data.account.administrator) {
      await dashboard.RedisList.remove('administrator:accounts', req.query.accountid)
    }
    if (req.data.account.deleted) {
      await dashboard.RedisList.remove(`deleted:accounts`, req.query.accountid)
    }
    req.success = true
  }
}
