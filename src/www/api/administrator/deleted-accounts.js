const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    const offset = req.query && req.query.offset ? parseInt(req.query.offset, 10) : 0
    const accountids = await dashboard.RedisList.list(`deleted:accounts`, offset)
    if (!accountids || !accountids.length) {
      return null
    }
    const accounts = await dashboard.Account.loadMany(accountids)
    for (const account of accounts) {
      delete (account.sessionKey)
      delete (account.usernameHash)
      delete (account.passwordHash)
    }
    return accounts
  }
}
