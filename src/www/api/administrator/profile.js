const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.profileid) {
      throw new Error('invalid-profileid')
    }
    const profile = await dashboard.Profile.load(req.query.profileid)
    if (!profile) {
      throw new Error('invalid-profileid')
    }
    return profile
  }
}
