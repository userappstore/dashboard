/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/administrator/profile`, () => {
  describe('Profile#GET', () => {
    it('should return specified profile data', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/profile?profileid=${user.profile.profileid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const profile = await req.route.api.get(req)
      assert.strictEqual(profile.profileid, user.account.profileid)
    })
  })
})
