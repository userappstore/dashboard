const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let profileids
    if (req.query.all) {
      profileids = await dashboard.RedisList.listAll(`profiles`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      profileids = await dashboard.RedisList.list(`profiles`, offset)
    }
    if (!profileids || !profileids.length) {
      return null
    }
    return dashboard.Profile.loadMany(profileids)
  }
}
