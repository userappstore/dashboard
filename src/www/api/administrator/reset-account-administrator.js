const dashboard = require('../../../../index.js')

module.exports = {
  /**
   * Revoke an user by DELETEing the accountid, then
   * completing an authorization and DELETEing again to apply
   * the queued change
   */
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    if (account.deleted || !account.administrator) {
      throw new Error('invalid-account')
    }
  },
  patch: async (req) => {
    await dashboard.Account.revokeAdministrator(req.query.accountid)
    await dashboard.RedisList.remove('administrator:accounts', req.query.accountid)
    req.success = true
    return dashboard.Account.load(req.query.accountid)
  }
}
