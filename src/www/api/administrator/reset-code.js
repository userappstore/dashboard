const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.codeid) {
      throw new Error('invalid-codeid')
    }
    const code = await dashboard.ResetCode.load(req.query.codeid)
    if (!code) {
      throw new Error('invalid-reset-code')
    }
    delete (code.codeHash)
    return code
  }
}
