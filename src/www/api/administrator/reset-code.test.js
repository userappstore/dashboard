/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/administrator/reset-code`, () => {
  describe('ResetCode#GET', () => {
    it('should return reset code data', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest(`/api/administrator/reset-code?codeid=${user.resetCode.codeid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const codeNow = await req.route.api.get(req)
      assert.strictEqual(codeNow.accountid, user.account.accountid)
    })

    it('should redact code hash', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.createResetCode(user)
      const req = TestHelper.createRequest(`/api/administrator/reset-code?codeid=${user.resetCode.codeid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      const codeNow = await req.route.api.get(req)
      assert.strictEqual(codeNow.accountid, user.account.accountid)
      assert.strictEqual(undefined, codeNow.code)
    })
  })
})
