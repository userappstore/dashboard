const dashboard = require('../../../../index.js')

module.exports = {
  patch: async (req) => {
    if (!req.query || !req.query.sessionid) {
      throw new Error('invalid-sessionid')
    }
    if (req.query.sessionid !== req.administratorSession.sessionid) {
      throw new Error('invalid-session')
    }
    await dashboard.Session.end(req.session.sessionid)
    await dashboard.RedisObject.removeProperty(req.administratorSession.sessionid, 'impersonate')
    req.success = true
    return dashboard.Session.load(req.session.sessionid)
  }
}
