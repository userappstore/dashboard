/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/administrator/reset-session-impersonate`, () => {
  describe('ResetSessionImpersonate#PATCH', () => {
    it('should remove impersonation from session', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      const req = TestHelper.createRequest(`/api/administrator/reset-session-impersonate?sessionid=${administrator.session.sessionid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = administrator.administratorSession
      req.administratorSession = req.session = administrator.session
      const sessionNow = await req.route.api.patch(req)
      assert.strictEqual(sessionNow.impersonate, undefined)
    })

    it('should end impersonated session', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      const req = TestHelper.createRequest(`/api/administrator/reset-session-impersonate?sessionid=${administrator.session.sessionid}`, 'GET')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = administrator.administratorSession
      req.administratorSession = req.session = administrator.session
      const sessionNow = await req.route.api.patch(req)
      assert.notStrictEqual(sessionNow.ended, undefined)
      assert.notStrictEqual(sessionNow.ended, null)
    })
  })
})
