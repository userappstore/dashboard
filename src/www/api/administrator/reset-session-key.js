const dashboard = require('../../../../index.js')
module.exports = {
  /**
   * End all of a user's sessions by generating a new
   * session key that invalidates all previous sessions
   */
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    if (account.deleted) {
      throw new Error('invalid-account')
    }
  },
  patch: async (req) => {
    await dashboard.Account.regenerateSessionKey(req.query.accountid)
    req.success = true
    const account = await dashboard.Account.load(req.query.accountid)
    return account
  }
}
