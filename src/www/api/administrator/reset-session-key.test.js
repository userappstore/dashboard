/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/administrator/reset-session-key`, () => {
  describe('ResetSessionKey#PATCH', () => {
    it('should apply authorized new session key', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/reset-session-key?accountid=${user.account.accountid}`, 'PATCH')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.patch(req)
      req.administratorSession = req.session = await TestHelper.unlockSession(administrator)
      const accountNow = await req.route.api.patch(req)
      assert.notStrictEqual(accountNow.sessionKey, user.account.sessionKey)
    })

    it('should update sessionKey_lastReset', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/reset-session-key?accountid=${user.account.accountid}`, 'PATCH')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.patch(req)
      req.administratorSession = req.session = await TestHelper.unlockSession(administrator)
      const accountNow = await req.route.api.patch(req)
      assert.notStrictEqual(accountNow.sessionKey_lastReset, undefined)
      assert.notStrictEqual(accountNow.sessionKey_lastReset, null)
    })
  })
})
