const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.sessionid) {
      throw new Error('invalid-sessionid')
    }
    const session = await dashboard.Session.load(req.query.sessionid)
    if (!session) {
      throw new Error('invalid-sessionid')
    }
    delete (session.tokenHash)
    return session
  }
}
