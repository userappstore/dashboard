const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    req.query = req.query || {}
    let sessionids
    if (req.query.all) {
      sessionids = await dashboard.RedisList.listAll(`sessions`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      sessionids = await dashboard.RedisList.list(`sessions`, offset)
    }
    if (!sessionids || !sessionids.length) {
      return null
    }
    const sessions = await dashboard.Session.loadMany(sessionids)
    const active = []
    if (sessions && sessions.length) {
      for (const session of sessions) {
        if (session.ended) {
          continue
        }
        delete (session.tokenHash)
        active.push(session)
      }
    }
    return active
  }
}
