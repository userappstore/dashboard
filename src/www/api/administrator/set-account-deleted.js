const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    if (account.deleted) {
      throw new Error('invalid-account')
    }
  },
  patch: async (req) => {
    await dashboard.Account.scheduleDelete(req.query.accountid)
    await dashboard.RedisList.add(`deleted:accounts`, req.query.accountid)
    req.success = true
    return dashboard.Account.load(req.query.accountid)
  }
}
