/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe('/api/administrator/set-account-deleted', () => {
  describe('SetAccountDeleted#patch', () => {
    it('should schedule for immediate deletion', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/set-account-deleted?accountid=${user.account.accountid}`, 'PATCH')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.patch(req)
      req.administratorSession = req.session = await TestHelper.unlockSession(administrator)
      global.deleteDelay = 0
      const accountNow = await req.route.api.patch(req)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((accountNow.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, 0)
    })

    it('should schedule for future deletion', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/set-account-deleted?accountid=${user.account.accountid}`, 'PATCH')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      await req.route.api.patch(req)
      req.administratorSession = req.session = req.session = await TestHelper.unlockSession(administrator)
      const accountNow = await req.route.api.patch(req)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((accountNow.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, global.deleteDelay)
    })
  })
})
