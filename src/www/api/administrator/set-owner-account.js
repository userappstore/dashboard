const dashboard = require('../../../../index.js')

module.exports = {
  /**
   * Transfer the ownership by PATCHing the session, then
   * completing an authorization and PATCHing again to apply
   * the queued change
   */
  lock: true,
  before: async (req) => {
    if (!req.account.owner) {
      throw new Error('invalid-account')
    }
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    if (account.deleted) {
      throw new Error('invalid-account')
    }
  },
  patch: async (req) => {
    await dashboard.RedisObject.setProperty(req.query.accountid, 'owner', dashboard.Timestamp.now)
    await dashboard.RedisObject.removeProperty(req.account.accountid, 'owner')
    req.success = true
    const account = dashboard.Account.load(req.query.accountid)
    return account
  }
}
