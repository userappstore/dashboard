/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe('/api/administrator/set-owner-account', () => {
  describe('SetOwnerAccount#PATCH', () => {
    it('should apply authorized new ownership', async () => {
      const owner = await TestHelper.createOwner()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/set-owner-account?accountid=${user.account.accountid}`, 'PATCH')
      req.administratorAccount = req.account = owner.account
      req.administratorSession = req.session = owner.session
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(owner)
      const accountNow = await req.route.api.patch(req)
      assert.notStrictEqual(accountNow.owner, undefined)
      assert.notStrictEqual(accountNow.owner, null)
    })

    it('should revoke own ownership', async () => {
      const owner = await TestHelper.createOwner()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/set-owner-account?accountid=${user.account.accountid}`, 'PATCH')
      req.administratorAccount = req.account = owner.account
      req.administratorSession = req.session = owner.session
      await req.route.api.patch(req)
      req.administratorSession = req.session = await TestHelper.unlockSession(owner)
      const userAccountNow = await req.route.api.patch(req)
      const req2 = TestHelper.createRequest(`/api/administrator/account?accountid=${owner.account.accountid}`, 'GET')
      req2.administratorAccount = req2.account = req.account
      req2.administratorSession = req2.session = req.session
      const ownerAccountNow = await req2.route.api.get(req2)
      assert.strictEqual(ownerAccountNow.owner, undefined)
      assert.notStrictEqual(userAccountNow.owner, undefined)
      assert.notStrictEqual(userAccountNow.owner, null)
    })
  })
})
