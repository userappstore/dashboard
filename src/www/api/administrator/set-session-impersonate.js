const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.sessionid) {
      throw new Error('invalid-sessionid')
    }
    if (req.query.sessionid !== req.session.sessionid) {
      throw new Error('invalid-session')
    }
    if (!req.body || !req.body.accountid) {
      throw new Error('invalid-accountid')
    }
    const account = await dashboard.Account.load(req.body.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    if (account.deleted) {
      throw new Error('invalid-account')
    }
  },
  patch: async (req) => {
    const newSession = await dashboard.Session.create(req.body.accountid)
    await dashboard.RedisObject.setProperties(newSession.sessionid, { ip: req.ip, userAgent: req.userAgent, 'administrator': req.administratorAccount.accountid })
    await dashboard.RedisObject.setProperty(req.query.sessionid, 'impersonate', newSession.sessionid)
    req.success = true
    return dashboard.Session.load(req.query.sessionid)
  }
}
