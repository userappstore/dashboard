/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/administrator/set-session-impersonate`, () => {
  describe('SetSessionImpersonate#PATCH', () => {
    it('should set authorized impersonate', async () => {
      const administrator = await TestHelper.createAdministrator()
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/administrator/set-session-impersonate?sessionid=${administrator.session.sessionid}`, 'PATCH')
      req.administratorAccount = req.account = administrator.account
      req.administratorSession = req.session = administrator.session
      req.body = {
        accountid: user.account.accountid
      }
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(administrator)
      const sessionNow = await req.route.api.patch(req)
      assert.notStrictEqual(sessionNow.impersonate, undefined)
      assert.notStrictEqual(sessionNow.impersonate, null)
    })
  })
})
