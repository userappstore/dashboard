const dashboard = require('../../../../index.js')

module.exports = {
  auth: false,
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (!req.session && !req.account) {
      throw new Error('invalid-account')
    }
    const account = await dashboard.Account.load(req.query.accountid)
    if (!account) {
      throw new Error('invalid-accountid')
    }
    if (req.account && req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (req.session && req.query.accountid !== req.session.accountid) {
      throw new Error('invalid-account')
    }
    if (req.query.accountid !== account.accountid) {
      throw new Error('invalid-account')
    }
    delete (account.sessionKey)
    delete (account.usernameHash)
    delete (account.passwordHash)
    return account
  }
}
