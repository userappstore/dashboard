/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/user/account`, () => {
  describe('Account#GET', () => {
    it('should reject invalid accountid', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/account?accountid=invalid`, 'GET')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.get(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-accountid')
    })

    it('should reject other accountid', async () => {
      const user = await TestHelper.createUser()
      const user2 = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/account?accountid=${user2.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.get(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should return account data', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/account?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const account = await req.route.api.get(req)
      assert.strictEqual(account.accountid, user.account.accountid)
    })

    it('should redact username, password, sessionKey', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/account?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const account = await req.route.api.get(req)
      assert.strictEqual(account.accountid, user.account.accountid)
      assert.strictEqual(undefined, account.usernameHash)
      assert.strictEqual(undefined, account.passwordHash)
      assert.strictEqual(undefined, account.sessionKey)
    })
  })
})
