const dashboard = require('../../../../index.js')

module.exports = {
  auth: false,
  post: async (req) => {
    if (!req || !req.body) {
      throw new Error('invalid-username')
    }
    if (!req.body.username || !req.body.username.length) {
      throw new Error('invalid-username')
    }
    if (!req.body.password || !req.body.password.length) {
      throw new Error('invalid-password')
    }
    if (global.minimumUsernameLength > req.body.username.length) {
      throw new Error('invalid-username-length')
    }
    if (global.minimumPasswordLength > req.body.password.length) {
      throw new Error('invalid-password-length')
    }
    const accountid = await dashboard.Account.create(req.body.username, req.body.password)
    if (!accountid) {
      throw new Error('unknown-error')
    }
    const profile = await dashboard.Profile.create(accountid)
    await dashboard.RedisObject.setProperty(accountid, 'profileid', profile.profileid)
    const account = await dashboard.Account.load(accountid)
    await dashboard.RedisList.add('accounts', accountid)
    if (account.administrator) {
      await dashboard.RedisList.add('administrator:accounts', accountid)
    }
    await dashboard.RedisList.add(`profiles`, profile.profileid)
    await dashboard.RedisList.add(`account:profiles:${accountid}`, profile.profileid)
    return account
  }
}
