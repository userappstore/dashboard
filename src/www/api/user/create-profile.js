const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (!req.body || !req.body.email || !req.body.email.length) {
      throw new Error('invalid-profile-email')
    }
    if (!req.body.firstName || !req.body.firstName.length) {
      throw new Error('invalid-profile-first-name')
    }
    if (global.minimumProfileFirstNameLength > req.body.firstName.length ||
        global.maximumProfileFirstNameLength < req.body.firstName.length) {
      throw new Error('invalid-profile-first-name-length')
    }
    if (!req.body.lastName || !req.body.lastName.length) {
      throw new Error('invalid-profile-last-name')
    }
    if (global.minimumProfileLastNameLength > req.body.lastName.length ||
        global.maximumProfileLastNameLength < req.body.lastName.length) {
      throw new Error('invalid-profile-last-name-length')
    }
  },
  post: async (req) => {
    const profile = await dashboard.Profile.create(req.query.accountid)
    await dashboard.RedisObject.setProperties(profile.profileid, {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email
    })
    await dashboard.RedisList.add(`profiles`, profile.profileid)
    await dashboard.RedisList.add(`account:profiles:${req.query.accountid}`, profile.profileid)
    if (req.body.default === 'true') {
      await dashboard.RedisObject.setProperty(req.query.accountid, 'profileid', profile.profileid)
    }
    req.success = true
    return dashboard.Profile.load(profile.profileid)
  }
}
