/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../../index.js')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/user/create-profile`, () => {
  describe('UpdateProfile#POST', () => {
    it('should reject missing first name', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: null,
        lastName: 'Test',
        email: 'test@email.com'
      }
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-first-name')
    })

    it('should enforce first name length', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: '1',
        lastName: 'Test',
        email: 'test@email.com'
      }
      global.minimumProfileFirstNameLength = 10
      global.maximumProfileFirstNameLength = 100
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-first-name-length')
      global.minimumProfileFirstNameLength = 1
      global.maximumProfileFirstNameLength = 1
      req.body = {
        firstName: '123456789',
        lastName: 'Test',
        email: 'test@email.com'
      }
      errorMessage = null
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-first-name-length')
    })

    it('should reject missing last name', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: null,
        email: 'test@email.com'
      }
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-last-name')
    })

    it('should enforce last name length', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: '1',
        email: 'test@email.com'
      }
      global.minimumProfileLastNameLength = 10
      global.maximumProfileLastNameLength = 100
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-last-name-length')
      global.minimumProfileLastNameLength = 1
      global.maximumProfileLastNameLength = 1
      req.body = {
        firstName: 'Test',
        lastName: '123456789',
        email: 'test@email.com'
      }
      errorMessage = null
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-last-name-length')
    })

    it('should reject missing email', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: 'Test',
        email: null
      }
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-profile-email')
    })

    it('should create authorized new profile', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: 'Person',
        email: 'test@test.com'
      }
      await req.route.api.post(req)
      req.session = await TestHelper.unlockSession(user)
      const profile = await req.route.api.post(req)
      assert.strictEqual(profile.firstName, req.body.firstName)
      assert.strictEqual(profile.lastName, req.body.lastName)
      assert.strictEqual(profile.email, req.body.email)
    })

    it('should create authorized new profile and set as default', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        firstName: 'Test',
        lastName: 'Person',
        email: 'test@test.com',
        default: 'true'
      }
      await req.route.api.post(req)
      req.session = await TestHelper.unlockSession(user)
      const profile = await req.route.api.post(req)
      assert.strictEqual(profile.firstName, req.body.firstName)
      assert.strictEqual(profile.lastName, req.body.lastName)
      assert.strictEqual(profile.email, req.body.email)
      const account = await dashboard.Account.load(user.account.accountid)
      assert.strictEqual(account.profileid, profile.profileid)
    })
  })
})
