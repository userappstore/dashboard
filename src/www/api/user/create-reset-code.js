const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (req.body && req.body.codeHash) {
      return
    }
    if (!req.body || !req.body.code) {
      throw new Error('invalid-reset-code')
    }
    if (global.minimumResetCodeLength > req.body.code.length ||
      global.maximumResetCodeLength < req.body.code.length) {
      throw new Error('invalid-reset-code-length')
    }
    req.body.codeHash = dashboard.Hash.fixedSaltHash(req.body.code)
    delete (req.body.code)
  },
  post: async (req) => {
    const code = await dashboard.ResetCode.create(req.query.accountid, req.body.codeHash)
    if (!code) {
      throw new Error('unknown-error')
    }
    await dashboard.RedisList.add(`resetCodes`, code.codeid)
    await dashboard.RedisList.add(`account:resetCodes:${req.query.accountid}`, code.codeid)
    await dashboard.RedisObject.setProperties(code.codeid, { ip: req.ip, userAgent: req.userAgent })
    await dashboard.RedisObject.setProperty(req.query.accountid, 'resetCode_lastCreated', dashboard.Timestamp.now)
    req.success = true
    return dashboard.ResetCode.load(code.codeid)
  }
}
