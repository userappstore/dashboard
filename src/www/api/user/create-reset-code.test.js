/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../../index.js')
const TestHelper = require('../../../../test-helper.js')

describe('/api/user/create-reset-code', () => {
  describe('CreateResetCode#POST', () => {
    it('should enforce code length', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-reset-code?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        code: '1'
      }
      global.minimumResetCodeLength = 100
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-reset-code-length')
    })

    it('should create a usable code', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/create-reset-code?accountid=${user.account.accountid}`, 'POST')
      req.account = user.account
      req.session = user.session
      const code = 'this-is-the-code'
      req.body = { code }
      await req.route.api.post(req)
      req.session = await TestHelper.unlockSession(user)
      await req.route.api.post(req)
      let errorMessage
      try {
        await dashboard.ResetCode.useCode(user.account.accountid, code)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(undefined, errorMessage)
    })
  })
})
