const dashboard = require('../../../../index.js')

module.exports = {
  auth: false,
  post: async (req) => {
    if (!req || !req.body) {
      throw new Error('invalid-username')
    }
    if (!req.body.username || !req.body.username.length) {
      throw new Error('invalid-username')
    }
    if (!req.body.password || !req.body.password.length) {
      throw new Error('invalid-password')
    }
    if (global.minimumUsernameLength > req.body.username.length) {
      throw new Error('invalid-username-length')
    }
    if (global.minimumPasswordLength > req.body.password.length) {
      throw new Error('invalid-password-length')
    }
    let account = await dashboard.Account.authenticate(req.body.username, req.body.password)
    if (!account) {
      throw new Error('invalid-username')
    }
    if (account.deleted) {
      throw new Error('account-deleted')
    }
    let expireSeconds
    switch (req.body.remember) {
      case 'hours':
        expireSeconds = 8 * 60 * 60
        break
      case 'days':
        expireSeconds = 30 * 24 * 60 * 60
        break
      default:
        expireSeconds = 20 * 60
        break
    }
    const expires = dashboard.Timestamp.date(dashboard.Timestamp.now + expireSeconds)
    const session = await dashboard.Session.create(account.accountid, expires)
    await dashboard.RedisObject.setProperties(session.sessionid, { ip: req.ip, userAgent: req.userAgent })
    await dashboard.RedisObject.setProperty(account.accountid, 'lastSignedIn', dashboard.Timestamp.now)
    await dashboard.RedisList.add(`sessions`, session.sessionid)
    await dashboard.RedisList.add(`account:sessions:${account.accountid}`, session.sessionid)
    return session
  }
}
