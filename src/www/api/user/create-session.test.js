/* eslint-env mocha */
const TestHelper = require('../../../../test-helper.js')
const assert = require('assert')
const dashboard = require('../../../../index.js')

describe('/api/user/create-session', () => {
  describe('SignIn#POST', () => {
    it('should require a username', async () => {
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: '',
        password: ''
      }
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
    })

    it('should require a username length', async () => {
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: '1',
        password: 'password'
      }
      global.minimumUsernameLength = 100
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username-length')
    })

    it('should require a password', async () => {
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: 'username',
        password: ''
      }
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password')
    })

    it('should require a username length', async () => {
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: 'username',
        password: '1'
      }
      global.minimumPasswordLength = 100
      let errorMessage
      try {
        await req.route.api.post(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password-length')
    })

    it('should create session expiring in 20 minutes', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: user.username,
        password: user.password
      }
      const session = await req.route.api.post(req)
      const minutes = Math.ceil((session.expires - dashboard.Timestamp.now) / 60)
      assert.strictEqual(minutes, 20)
    })

    it('should create session expiring in 8 hours', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: user.username,
        password: user.password,
        remember: 'hours'
      }
      const session = await req.route.api.post(req)
      const hours = Math.ceil((session.expires - dashboard.Timestamp.now) / 60 / 60)
      assert.strictEqual(hours, 8)
    })

    it('should create session expiring in 30 days', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/api/user/create-session', 'POST')
      req.body = {
        username: user.username,
        password: user.password,
        remember: 'days'
      }
      const session = await req.route.api.post(req)
      const days = Math.ceil((session.expires - dashboard.Timestamp.now) / 60 / 60 / 24)
      assert.strictEqual(days, 30)
    })
  })
})
