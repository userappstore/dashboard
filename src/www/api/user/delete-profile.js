const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.profileid) {
      throw new Error('invalid-profileid')
    }
    if (req.query.profileid === req.account.profileid) {
      throw new Error('invalid-profile')
    }
    const profile = await dashboard.Profile.load(req.query.profileid)
    if (profile.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
  },
  delete: async (req) => {
    await dashboard.Profile.deleteProfile(req.query.profileid)
    await dashboard.RedisObject.setProperty(req.account.accountid, 'profile_lastDeleted', dashboard.Timestamp.now)
    await dashboard.RedisList.remove(`profiles`, req.query.profileid)
    await dashboard.RedisList.remove(`account:profiles:${req.account.accountid}`, req.query.profileid)
    const account = await dashboard.Account.load(req.account.accountid)
    req.success = true
    return account
  }
}
