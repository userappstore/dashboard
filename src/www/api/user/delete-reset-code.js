const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.codeid) {
      throw new Error('invalid-codeid')
    }
    const code = await dashboard.ResetCode.load(req.query.codeid)
    if (code.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
  },
  delete: async (req) => {
    await dashboard.ResetCode.deleteCode(req.query.codeid)
    await dashboard.RedisObject.setProperty(req.account.accountid, 'resetCode_lastDeleted', dashboard.Timestamp.now)
    await dashboard.RedisList.remove(`resetCodes`, req.query.codeid)
    await dashboard.RedisList.remove(`account:resetCodes:${req.account.accountid}`, req.query.codeid)
    const account = await dashboard.Account.load(req.account.accountid)
    req.success = true
    return account
  }
}
