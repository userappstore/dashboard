const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (req.query.all) {
      profileids = await dashboard.RedisList.listAll(`profiles`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      profileids = await dashboard.RedisList.list(`profiles`, offset)
    }
    if (!profileids || !profileids.length) {
      return null
    }
    const profiles = await dashboard.Profile.loadMany(profileids)
    return profiles
  }
}
