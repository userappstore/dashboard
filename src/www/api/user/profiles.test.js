/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/user/profiles`, () => {
  describe('Profiles#GET', () => {
    it('should limit profiles to one page', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.createProfile(user)
      const req = TestHelper.createRequest(`/api/user/profiles?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const profilesNow = await req.route.api.get(req)
      assert.strictEqual(profilesNow.length, global.pageSize)
    })

    it('should redact profile hash', async () => {
      const user = await TestHelper.createUser()
      const profile1 = user.profile
      await TestHelper.createProfile(user)
      const profile2 = user.profile
      const req = TestHelper.createRequest(`/api/user/profiles?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const profilesNow = await req.route.api.get(req)
      assert.strictEqual(profilesNow.length, global.pageSize)
      assert.strictEqual(profilesNow[0].profileid, profile2.profileid)
      assert.strictEqual(profilesNow[1].profileid, profile1.profileid)
    })

    it('should enforce page size', async () => {
      global.pageSize = 3
      const user = await TestHelper.createUser()
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProfile(user)
      }
      const req = TestHelper.createRequest(`/api/user/profiles?accountid=${user.account.accountid}`, 'GET')
      req.account = user.account
      req.session = user.session
      const profilesNow = await req.route.api.get(req)
      assert.strictEqual(profilesNow.length, global.pageSize)
    })

    it('should enforce specified offset', async () => {
      const offset = 1
      const user = await TestHelper.createUser()
      const profiles = [ user.profile ]
      for (let i = 0, len = offset + global.pageSize + 1; i < len; i++) {
        await TestHelper.createProfile(user)
        profiles.unshift(user.profile)
      }
      const req = TestHelper.createRequest(`/api/user/profiles?accountid=${user.account.accountid}&offset=${offset}`, 'GET')
      req.account = user.account
      req.session = user.session
      const profilesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize; i < len; i++) {
        assert.strictEqual(profilesNow[i].profileid, profiles[offset + i].profileid)
      }
    })

    it('should return all records', async () => {
      const user = await TestHelper.createUser()
      const profiles = [user.profile]
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        await TestHelper.createProfile(user)
        profiles.unshift(user.profile)
      }
      const req = TestHelper.createRequest(`/api/user/profiles?accountid=${user.account.accountid}&all=true`, 'GET')
      req.account = user.account
      req.session = user.session
      const profilesNow = await req.route.api.get(req)
      for (let i = 0, len = global.pageSize + 1; i < len; i++) {
        assert.strictEqual(profilesNow[i].profileid, profiles[i].profileid)
      }
    })
  })
})
