const dashboard = require('../../../../index.js')

module.exports = {
  auth: false,
  patch: async (req) => {
    if (!req.body) {
      throw new Error('invalid-username')
    }
    if (!req.body.username) {
      throw new Error('invalid-username')
    }
    if (!req.body.username || !req.body.username.length) {
      throw new Error('invalid-username')
    }
    if (global.minimumUsernameLength > req.body.username.length) {
      throw new Error('invalid-username-length')
    }
    if (!req.body.password || !req.body.password.length) {
      throw new Error('invalid-password')
    }
    if (global.minimumPasswordLength > req.body.password.length) {
      throw new Error('invalid-password-length')
    }
    const account = await dashboard.Account.authenticate(req.body.username, req.body.password)
    if (!account) {
      throw new Error('invalid-username')
    }
    if (!account.deleted) {
      throw new Error('invalid-account')
    }
    if (account.deleted < dashboard.Timestamp.now) {
      throw new Error('invalid-account')
    }
    await dashboard.Account.cancelDelete(account.accountid)
    await dashboard.RedisList.remove(`deleted:accounts`, account.accountid)
    const accountNow = dashboard.Account.load(account.accountid)
    return accountNow
  }
}
