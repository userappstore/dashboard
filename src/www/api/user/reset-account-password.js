const dashboard = require('../../../../index.js')

module.exports = {
  auth: false,
  patch: async (req) => {
    if (!req.body || !req.body.code) {
      throw new Error('invalid-reset-code')
    }
    if (!req.body.username) {
      throw new Error('invalid-username')
    }
    if (!req.body.username || !req.body.username.length ||
      global.minimumUsernameLength > req.body.username.length) {
      throw new Error('invalid-username')
    }
    if (!req.body.password || !req.body.password.length) {
      throw new Error('invalid-password')
    }
    if (global.minimumPasswordLength > req.body.password.length) {
      throw new Error('invalid-password-length')
    }
    if (!req.body.code || !req.body.code.length) {
      throw new Error('invalid-reset-code')
    }
    if (global.minimumResetCodeLength > req.body.code.length) {
      throw new Error('invalid-reset-code-length')
    }
    let account = await dashboard.Account.identify(req.body.username)
    if (!account) {
      throw new Error('invalid-username')
    }
    if (account.deleted) {
      throw new Error('invalid-account')
    }
    if (account.deleted < dashboard.Timestamp.now) {
      throw new Error('invalid-account')
    }
    const valid = await dashboard.ResetCode.useCode(account.accountid, req.body.code)
    if (!valid) {
      throw new Error('invalid-reset-code')
    }
    const passwordHash = dashboard.Hash.randomSaltHash(req.body.password)
    await dashboard.RedisObject.setProperty(account.accountid, 'passwordHash', passwordHash)
    await dashboard.RedisObject.setProperty(account.accountid, 'resetCode_lastUsed', dashboard.Timestamp.now)
    await dashboard.Account.regenerateSessionKey(account.accountid)
    return dashboard.Account.load(account.accountid)
  }
}
