/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe('/api/user/reset-account-password', () => {
  describe('ResetAccountPassword#POST()', () => {
    it('should require username', async () => {
      const req = TestHelper.createRequest('/api/user/reset-account-password', 'POST')
      req.body = {
        username: '',
        password: 'password',
        code: 'code'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
    })

    it('should require new password', async () => {
      const req = TestHelper.createRequest('/api/user/reset-account-password', 'POST')
      req.body = {
        username: 'username',
        password: '',
        code: 'code'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password')
    })

    it('should require enforce password requirements', async () => {
      const req = TestHelper.createRequest('/api/user/reset-account-password', 'POST')
      req.body = {
        username: 'username',
        password: 'short',
        code: 'code'
      }
      global.minimumPasswordLength = 100
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password-length')
    })

    it('should require reset code', async () => {
      const req = TestHelper.createRequest('/api/user/reset-account-password', 'POST')
      req.body = {
        username: 'username',
        password: 'password'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-reset-code')
    })

    it('should require valid reset code', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/api/user/reset-account-password', 'POST')
      req.body = {
        username: user.username,
        password: user.password,
        code: 'invalid'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-reset-code')
    })
  })
})
