const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.codeid) {
      throw new Error('invalid-codeid')
    }
    const code = await dashboard.ResetCode.load(req.query.codeid)
    if (!code) {
      throw new Error('invalid-codeid')
    }
    if (code.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    delete (code.codeHash)
    return code
  }
}
