const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    let codeids
    if (req.query.all) {
      codeids = await dashboard.RedisList.listAll(`resetCodes`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      codeids = await dashboard.RedisList.list(`resetCodes`, offset)
    }
    if (!codeids || !codeids.length) {
      return null
    }
    const resetCodes = await dashboard.ResetCode.loadMany(codeids)
    for (const code of resetCodes) {
      delete (code.codeHash)
    }
    return resetCodes
  }
}
