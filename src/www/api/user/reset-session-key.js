const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  patch: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-session')
    }
    await dashboard.Account.regenerateSessionKey(req.account.accountid)
    const account = await dashboard.Account.load(req.account.accountid)
    req.success = true
    return account
  }
}
