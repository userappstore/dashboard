/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../../index.js')
const TestHelper = require('../../../../test-helper.js')

/* eslint-env mocha */
describe('/api/user/reset-session-key', async () => {
  describe('ResetSessionKey#PATCH', () => {
    it('should end current session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/reset-session-key?accountid=${user.account.accountid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      await req.route.api.patch(req)
      const session = await dashboard.Session.load(user.session.sessionid)
      assert.notStrictEqual(session.ended, undefined)
      assert.notStrictEqual(session.ended, null)
    })

    it('should update account sessionKey_lastReset', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/reset-session-key?accountid=${user.account.accountid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const account = await req.route.api.patch(req)
      assert.notStrictEqual(account.sessionKey_lastReset, undefined)
      assert.notStrictEqual(account.sessionKey_lastReset, null)
    })
  })
})
