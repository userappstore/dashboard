const dashboard = require('../../../../index.js')

module.exports = {
  patch: async (req) => {
    if (!req.query || !req.query.sessionid) {
      throw new Error('invalid-sessionid')
    }
    if (req.query.sessionid !== req.session.sessionid) {
      throw new Error('invalid-session')
    }
    const session = await dashboard.Session.load(req.query.sessionid)
    if (!session.unlocked) {
      throw new Error('invalid-session')
    }
    await dashboard.RedisObject.removeProperty(req.session.sessionid, 'unlocked')
    return dashboard.Session.load(req.query.sessionid)
  }
}
