const dashboard = require('../../../../index.js')

module.exports = {
  get: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.query.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    let sessionids
    if (req.query.all) {
      sessionids = await dashboard.RedisList.listAll(`account:sessions:${req.query.accountid}`)
    } else {
      const offset = req.query.offset ? parseInt(req.query.offset, 10) : 0
      sessionids = await dashboard.RedisList.list(`account:sessions:${req.query.accountid}`, offset)
    }
    if (!sessionids || !sessionids.length) {
      return null
    }
    const sessions = await dashboard.Session.loadMany(sessionids)
    for (const session of sessions) {
      delete (session.tokenHash)
    }
    return sessions
  }
}
