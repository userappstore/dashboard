const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  patch: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    await dashboard.Account.scheduleDelete(req.query.accountid)
    await dashboard.RedisList.add(`deleted:accounts`, req.query.accountid)
    const account = await dashboard.Account.load(req.query.accountid)
    req.success = true
    return account
  }
}
