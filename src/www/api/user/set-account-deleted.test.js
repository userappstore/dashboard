/* eslint-env mocha */
const assert = require('assert')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/user/set-account-deleted`, () => {
  describe('DeleteAccount#DELETE', () => {
    it('should schedule deletion in 7 days', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-account-deleted?accountid=${user.account.accountid}`, 'DELETE')
      req.account = user.account
      req.session = user.session
      global.deleteDelay = 7
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((accountNow.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, 7)
    })

    it('should schedule deletion in 3 days', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-account-deleted?accountid=${user.account.accountid}`, 'DELETE')
      req.account = user.account
      req.session = user.session
      global.deleteDelay = 3
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((accountNow.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, 3)
    })

    it('should schedule immediate deletion', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-account-deleted?accountid=${user.account.accountid}`, 'DELETE')
      req.account = user.account
      req.session = user.session
      global.deleteDelay = 0
      await req.route.api.patch(req)
      req.session = await TestHelper.unlockSession(user)
      const accountNow = await req.route.api.patch(req)
      const now = Math.floor(new Date().getTime() / 1000)
      const days = Math.ceil((accountNow.deleted - now) / 60 / 60 / 24)
      assert.strictEqual(days, 0)
    })
  })
})
