const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    if (!req.body || !req.body.password) {
      throw new Error('invalid-password')
    }
    if (global.minimumPasswordLength > req.body.password.length ||
      global.maximumPasswordLength < req.body.password.length) {
      throw new Error('invalid-password-length')
    }
    req.body = {
      password: dashboard.Hash.randomSaltHash(req.body.password)
    }
  },
  patch: async (req) => {
    await dashboard.RedisObject.setProperty(req.account.accountid, 'passwordHash', req.body.password)
    const account = await dashboard.Account.load(req.account.accountid)
    req.success = true
    return account
  }
}
