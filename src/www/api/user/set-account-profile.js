const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    if (!req.body || !req.body.profileid) {
      throw new Error('invalid-profileid')
    }
    const profile = await dashboard.Profile.load(req.body.profileid)
    if (!profile) {
      throw new Error('invalid-profileid')
    }
    if (profile.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    if (req.account.profileid === profile.profileid) {
      throw new Error('invalid-profile')
    }
  },
  patch: async (req) => {
    await dashboard.RedisObject.setProperty(req.account.accountid, 'profileid', req.body.profileid)
    const account = await dashboard.Account.load(req.account.accountid)
    req.success = true
    return account
  }
}
