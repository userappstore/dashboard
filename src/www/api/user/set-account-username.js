const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.accountid) {
      throw new Error('invalid-accountid')
    }
    if (req.account.accountid !== req.query.accountid) {
      throw new Error('invalid-account')
    }
    if (!req.body || !req.body.username) {
      throw new Error('invalid-username')
    }
    if (global.minimumUsernameLength > req.body.username.length ||
      global.maximumUsernameLength < req.body.username.length) {
      throw new Error('invalid-username-length')
    }
    req.body.username = dashboard.Hash.fixedSaltHash(req.body.username)
  },
  patch: async (req) => {
    await dashboard.Account.changeUsername(req.query.accountid, req.body.username)
    req.success = true
    const account = await dashboard.Account.load(req.query.accountid)
    return account
  }
}
