const dashboard = require('../../../../index.js')

module.exports = {
  before: async (req) => {
    if (!req.query || !req.query.sessionid) {
      throw new Error('invalid-sessionid')
    }
    const session = await dashboard.Session.load(req.query.sessionid)
    if (!session) {
      throw new Error('invalid-sessionid')
    }
    if (session.ended) {
      throw new Error('invalid-session')
    }
    if (session.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
  },
  patch: async (req) => {
    await dashboard.Session.end(req.query.sessionid)
    const session = await dashboard.Session.load(req.query.sessionid)
    return session
  }
}
