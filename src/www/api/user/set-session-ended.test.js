/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../../index.js')
const TestHelper = require('../../../../test-helper.js')

/* eslint-env mocha */
describe(`/api/user/set-session-ended`, () => {
  describe('SetSessionEnded#PATCH', () => {
    it('should reject invalid sessionid', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest('/api/user/set-session-ended?sessionid=invalid', 'PATCH')
      req.account = user.account
      req.session = user.session
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-sessionid')
    })

    it('should reject ended session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-session-ended?sessionid=${user.session.sessionid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      await req.route.api.patch(req)
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-session')
    })

    it('should accept locked session', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const lockedSession = user.session
      const req = TestHelper.createRequest(`/api/user/set-session-ended?sessionid=${lockedSession.sessionid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      const sessionNow = await req.route.api.patch(req)
      assert.strictEqual(dashboard.Timestamp.now, sessionNow.ended)
    })

    it('should end the session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-session-ended?sessionid=${user.session.sessionid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      const sessionNow = await req.route.api.patch(req)
      assert.strictEqual(dashboard.Timestamp.now, sessionNow.ended)
    })
  })
})
