const dashboard = require('../../../../index.js')

module.exports = {
  patch: async (req) => {
    if (!req.session.lock) {
      throw new Error('invalid-session')
    }
    if (!req.query || !req.query.sessionid) {
      throw new Error('invalid-sessionid')
    }
    if (req.session.sessionid !== req.query.sessionid) {
      throw new Error('invalid-session')
    }
    if (!req.body) {
      throw new Error('invalid-username')
    }
    if (!req.body.username || !req.body.username.length) {
      throw new Error('invalid-username')
    }
    if (!req.body.password || !req.body.password.length) {
      throw new Error('invalid-password')
    }
    if (global.minimumUsernameLength > req.body.username.length) {
      throw new Error('invalid-username-length')
    }
    if (global.minimumPasswordLength > req.body.password.length) {
      throw new Error('invalid-password-length')
    }
    const account = await dashboard.Account.authenticate(req.body.username, req.body.password)
    if (!account) {
      throw new Error('invalid-username')
    }
    if (account.deleted) {
      throw new Error('invalid-account')
    }
    if (req.administratorAccount) {
      if (req.administratorAccount.accountid !== account.accountid) {
        throw new Error('invalid-account')
      }
    } else if (account.accountid !== req.account.accountid) {
      throw new Error('invalid-account')
    }
    await dashboard.Session.unlock(req.session.sessionid, req.body.remember === 'minutes')
    const lockURL = req.session.lockURL
    const session = await dashboard.Session.load(req.session.sessionid)
    session.lockURL = lockURL
    return session
  }
}
