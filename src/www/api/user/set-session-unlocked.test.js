/* eslint-env mocha */
const assert = require('assert')
const dashboard = require('../../../../index.js')
const TestHelper = require('../../../../test-helper.js')

describe(`/api/user/set-session-unlocked`, () => {
  describe('Authenticate#POST', () => {
    it('should require a locked session', async () => {
      const user = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: 'username',
        password: 'password'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-session')
    })

    it('should require a username', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: '',
        password: 'password'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
    })

    it('should require a password', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: 'username',
        password: ''
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password')
    })

    it('should require a valid username and password', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: 'username',
        password: 'password'
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-username')
      req.body = {
        username: user.username,
        password: 'password'
      }
      errorMessage = null
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-password')
    })

    it('should reject other account username and password', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const user2 = await TestHelper.createUser()
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'POST')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user2.username,
        password: user2.password
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should require impersonating administrator use own credentials', async () => {
      const user = await TestHelper.createUser()
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      await TestHelper.lockSession(administrator)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${administrator.session.sessionid}`, 'POST')
      req.administratorAccount = administrator.administratorAccount
      req.administratorSession = administrator.administratorSession
      req.account = administrator.account
      req.session = administrator.session
      req.body = {
        username: user.username,
        password: user.password
      }
      let errorMessage
      try {
        await req.route.api.patch(req)
      } catch (error) {
        errorMessage = error.message
      }
      assert.strictEqual(errorMessage, 'invalid-account')
    })

    it('should unlock locked session for user', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user.username,
        password: user.password
      }
      const sessionNow = await req.route.api.patch(req)
      assert.notStrictEqual(sessionNow.unlocked, undefined)
      assert.notStrictEqual(sessionNow.unlocked, null)
    })

    it('should unlock session for impersonating administrator', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const administrator = await TestHelper.createAdministrator()
      await TestHelper.setImpersonate(administrator, user.account.accountid)
      await TestHelper.lockSession(administrator)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${administrator.session.sessionid}`, 'POST')
      req.administratorAccount = administrator.administratorAccount
      req.administratorSession = administrator.administratorSession
      req.account = administrator.account
      req.session = administrator.session
      req.body = {
        username: administrator.username,
        password: administrator.password
      }

      const sessionNow = await req.route.api.patch(req)
      assert.notStrictEqual(sessionNow.unlocked, undefined)
      assert.notStrictEqual(sessionNow.unlocked, null)
    })

    it('should skip authorization while unlocked', async () => {
      const user = await TestHelper.createUser()
      await TestHelper.lockSession(user)
      const req = TestHelper.createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'PATCH')
      req.account = user.account
      req.session = user.session
      req.body = {
        username: user.username,
        password: user.password,
        remember: 'minutes'
      }
      const session = await req.route.api.patch(req)
      const req2 = TestHelper.createRequest(`/api/user/set-account-username?accountid=${user.account.accountid}`, 'PATCH')
      req2.account = user.account
      req2.session = session
      req2.body = {
        username: 'new-username-' + new Date().getTime() + '-' + Math.floor((Math.random() * 100000))
      }
      await req2.route.api.patch(req2)
      const account = await dashboard.Account.load(user.account.accountid)
      assert.notStrictEqual(account.usernameHash, user.account.usernameHash)
    })
  })
})
