const dashboard = require('../../../../index.js')

module.exports = {
  lock: true,
  before: async (req) => {
    if (!req.query || !req.query.profileid) {
      throw new Error('invalid-profileid')
    }
    const profile = await global.api.user.Profile.get(req)
    if (!profile) {
      throw new Error('invalid-profileid')
    }
    if (!req.body || !req.body.email || !req.body.email.length) {
      throw new Error('invalid-profile-email')
    }
    if (!req.body.firstName || !req.body.firstName.length) {
      throw new Error('invalid-profile-first-name')
    }
    if (global.minimumProfileFirstNameLength > req.body.firstName.length ||
        global.maximumProfileFirstNameLength < req.body.firstName.length) {
      throw new Error('invalid-profile-first-name-length')
    }
    if (!req.body.lastName || !req.body.lastName.length) {
      throw new Error('invalid-profile-last-name')
    }
    if (global.minimumProfileLastNameLength > req.body.lastName.length ||
        global.maximumProfileLastNameLength < req.body.lastName.length) {
      throw new Error('invalid-profile-last-name-length')
    }
  },
  patch: async (req) => {
    await dashboard.RedisObject.setProperties(req.query.profileid, {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email
    })
    req.success = true
    return dashboard.Profile.load(req.query.profileid)
  }
}
