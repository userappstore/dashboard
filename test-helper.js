/* eslint-env mocha */
const bcrypt = require('bcrypt-node')
const dashboard = require('./index.js')
const testData = require('./test-data.json')
const url = require('url')

let testDataIndex = 0

before(async () => {
  await dashboard.start(global.applicationPath || __dirname)
})

beforeEach(async () => {
  global.appid = `tests_${dashboard.Timestamp.now}`
  global.uuidIncrement = 1
  global.uuidSeed = dashboard.Timestamp.now
  global.testNumber = dashboard.Timestamp.now
  global.testModuleJSON = null
  global.minimumUsernameLength = 1
  global.maximumUsernameLength = 100
  global.minimumPasswordLength = 1
  global.maximumPasswordLength = 100
  global.minimumResetCodeLength = 1
  global.maximumResetCodeLength = 100
  global.minimumProfileFirstNameLength = 1
  global.maximumProfileFirstNameLength = 100
  global.minimumProfileLastNameLength = 1
  global.maximumProfileLastNameLength = 100
  global.bcryptWorkloadFactor = 1
  global.deleteDelay = 7
  global.maximumProfileFieldLength = 50
  global.pageSize = 2
  global.uuidEncodingCharacters = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'
  global.allowPublicAPI = true
  global.bcryptFixedSalt = bcrypt.genSaltSync(1)
  await global.redisClient.flushdb()
  return global.redisClient.setnxAsync(`uuids`, global.uuidSeed)
})

after((callback) => {
  dashboard.stop()
  global.testEnded = true
  return callback()
})

module.exports = {
  createAdministrator,
  createOwner,
  createProfile,
  createRequest,
  createResponse,
  createSession,
  createUser,
  setDeleted,
  setImpersonate,
  lockSession,
  createResetCode,
  unlockSession,
  extractDoc
}

function createRequest (rawURL, method) {
  const req = {
    state: 'route',
    method: method.toUpperCase(),
    url: rawURL,
    urlPath: rawURL.split('?')[0],
    headers: {
      'User-Agent': 'Integration tests'
    },
    ip: '1.2.3.4',
    userAgent: 'Integration test'
  }
  if (rawURL.indexOf('?') > -1) {
    req.query = url.parse(rawURL, true).query
  }
  req.route = global.sitemap[req.urlPath]
  return req
}

function createResponse () {
  const headers = {}
  return {
    headers,
    statusCode: 0,
    setHeader: (name, value) => {
      headers[name] = value
    }
  }
}

function extractDoc (str) {
  let doc
  const templateDoc = dashboard.HTML.parse(str)
  const applicationIframe = templateDoc.getElementById('application-iframe')
  if (applicationIframe) {
    const pageSource = applicationIframe.attr.srcdoc.join(' ')
    doc = dashboard.HTML.parse(pageSource)
  } else {
    doc = templateDoc
  }
  return doc
}

async function createAdministrator () {
  const administrator = await createUser('administrator-' + dashboard.Timestamp.now + '-' + Math.ceil(Math.random() * 100000))
  // the 'unwrapped' API handlers have to be used here because in
  // normal use the first account is automatically made the owner
  // and administrator and then authorizes other acounts, in tests
  // they need to be created on demand
  if (!administrator.account.administrator) {
    const req2 = createRequest(`/api/administrator/set-account-administrator?accountid=${administrator.account.accountid}`, 'PATCH')
    await req2.route.api._patch(req2)
  }
  administrator.administratorAccount = administrator.account = await dashboard.Account.load(administrator.account.accountid)
  administrator.administratorSession = administrator.session = await dashboard.Session.load(administrator.session.sessionid)
  return administrator
}

async function createOwner () {
  const administrator = await createUser('owner-' + dashboard.Timestamp.now + '-' + Math.ceil(Math.random() * 100000))
  if (!administrator.account.administrator) {
    const req2 = createRequest(`/api/administrator/set-account-administrator?accountid=${administrator.account.accountid}`, 'PATCH')
    await req2.route.api._patch(req2)
  }
  if (!administrator.account.owner) {
    const req2 = createRequest(`/api/administrator/set-owner-account?accountid=${administrator.account.accountid}`, 'PATCH')
    await req2.route.api._patch(req2)
  }
  administrator.administratorAccount = administrator.account = await dashboard.Account.load(administrator.account.accountid)
  administrator.administratorSession = administrator.session = await dashboard.Session.load(administrator.session.sessionid)
  return administrator
}

async function createUser (username) {
  username = username || 'user-' + dashboard.Timestamp.now + '-' + Math.ceil(Math.random() * 100000)
  const password = username
  const req = createRequest('/api/user/create-account', 'POST')
  req.body = { username, password }
  let account = await req.route.api.post(req)
  const req2 = createRequest(`/api/user/create-session?accountid=${account.accountid}`, 'POST')
  req2.body = { username, password }
  let session = await req2.route.api.post(req2)
  const sessionToken = session.token
  const req3 = createRequest(`/api/user/update-profile?profileid=${account.profileid}`, 'PATCH')
  req3.account = account
  req3.session = session
  req3.body = {
    firstName: testData[testDataIndex].firstName,
    lastName: testData[testDataIndex].lastName,
    email: testData[testDataIndex].email
  }
  await req3.route.api.patch(req3)
  const user = { account, session, username, password }
  await unlockSession(user)
  req3.session = user.session
  await req3.route.api.patch(req3)
  user.profile = await dashboard.Profile.load(account.profileid)
  user.session = await dashboard.Session.load(session.sessionid)
  user.account = await dashboard.Account.load(account.accountid)
  user.session.token = sessionToken
  return user
}

async function createSession (user, expires) {
  const req = createRequest(`/api/user/create-session?accountid=${user.account.accountid}`, 'POST')
  req.body = {
    username: user.username,
    password: user.password,
    expires: expires || ''
  }
  const session = await req.route.api.post(req)
  user.session = session
  return session
}

async function lockSession (user) {
  user.session = await dashboard.Session.load(user.session.sessionid)
  if (user.session.lock) {
    throw new Error('invalid-session')
  }
  const req = createRequest(`/api/user/set-account-password?accountid=${user.account.accountid}`, 'PATCH')
  req.account = user.account
  req.session = user.session
  req.body = {
    password: user.password,
    confirm: user.password
  }
  await req.route.api.patch(req)
  const session = await dashboard.Session.load(user.session.sessionid)
  user.session = session
  return session
}

async function unlockSession (user, long) {
  user.session = await dashboard.Session.load(user.session.sessionid)
  if (!user.session.lock) {
    throw new Error('invalid-session')
  }
  const req = createRequest(`/api/user/set-session-unlocked?sessionid=${user.session.sessionid}`, 'PATCH')
  req.account = user.account
  req.session = user.session
  req.body = {
    username: user.username,
    password: user.password,
    remember: long ? 'minutes' : ''
  }
  await req.route.api.patch(req)
  user.session = await dashboard.Session.load(user.session.sessionid)
  return user.session
}

async function setDeleted (user) {
  user.session = await dashboard.Session.load(user.session.sessionid)
  const req = createRequest(`/api/user/set-account-deleted?accountid=${user.account.accountid}`, 'PATCH')
  req.account = user.account
  req.session = user.session
  await req.route.api.patch(req)
  await unlockSession(user)
  req.session = user.session
  await req.route.api.patch(req)
  const account = await dashboard.Account.load(user.account.accountid)
  user.account = account
  user.session = await dashboard.Session.load(user.session.sessionid)
  return account
}

async function setImpersonate (administrator, accountid) {
  const req = createRequest(`/api/administrator/set-session-impersonate?sessionid=${administrator.session.sessionid}`, 'PATCH')
  req.administratorAccount = req.account = administrator.account
  req.administratorSession = req.session = administrator.session
  req.body = { accountid }
  await req.route.api.patch(req)
  req.administratorSession = req.session = await unlockSession(administrator)
  administrator.administratorSession = req.administratorSession = await req.route.api.patch(req)
  const session = await dashboard.Session.load(req.administratorSession.impersonate)
  administrator.account = await dashboard.Account.load(accountid)
  administrator.session = session
  return session
}

async function createResetCode (user) {
  user.session = await dashboard.Session.load(user.session.sessionid)
  const code = 'resetCode-' + dashboard.Timestamp.now + '-' + Math.ceil(Math.random() * 100000)
  const req = createRequest(`/api/user/create-reset-code?accountid=${user.account.accountid}`, 'POST')
  req.account = user.account
  req.session = user.session
  req.body = { code }
  await req.route.api.post(req)
  await unlockSession(user)
  req.session = user.session
  const resetCode = await req.route.api.post(req)
  resetCode.code = code
  user.resetCode = resetCode
  user.account = await dashboard.Account.load(user.account.accountid)
  user.session = await dashboard.Session.load(user.session.sessionid)
  return resetCode
}

async function createProfile (user) {
  user.session = await dashboard.Session.load(user.session.sessionid)
  const req = createRequest(`/api/user/create-profile?accountid=${user.account.accountid}`, 'POST')
  req.account = user.account
  req.session = user.session
  req.body = {
    firstName: user.profile.firstName,
    lastName: user.profile.lastName,
    email: testData[testDataIndex].email,
    default: 'true'
  }
  testDataIndex++
  await req.route.api.post(req)
  await unlockSession(user)
  req.session = user.session
  const profile = await req.route.api.post(req)
  user.account = await dashboard.Account.load(user.account.accountid)
  user.session = await dashboard.Session.load(user.session.sessionid)
  user.profile = profile
  return profile
}
